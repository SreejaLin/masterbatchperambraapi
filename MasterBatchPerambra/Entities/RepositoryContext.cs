﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
namespace Entities
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
           : base(options)
        {
        }


        public DbSet<BNBCompoundAgingRelaxation> BNB_Compound_Aging_Relaxation { get; set; }
        public DbSet<BNBCompoundMaster> BNB_Compound_Master { get; set; }
        public DbSet<BNBCompoundTypeMaster> BNB_CompoundType_Master { get; set; }

        public DbSet<BNBHoldReasons> BNB_Hold_Reasons { get; set; }

        public DbSet<BNBLocationMaster> BNB_Location_Master { get; set; }
        public DbSet<BNBMaster> BNB_Master { get; set; }
        public DbSet<BNBProductionStatus> BNB_Production_Status { get; set; }
        public DbSet<BNBUsers> BNB_Users { get; set; }

        public DbSet<PlantMaster> PlantMaster { get; set; }
        public DbSet<BNBProductionEntry> BNB_Production_Entry { get; set; }
        public DbSet<BNBHold> BNB_Hold { get; set; }
        public DbSet<BNBTagGeneration> BNB_Tag_Generation { get; set; }
        public DbSet<BNBLogStatus> BNB_LogStatus { get; set; }
        public DbSet<BNBSupplier> BNB_Supplier { get; set; }
        public DbSet<BNBMaterialMapping> BNB_MaterialMapping { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
