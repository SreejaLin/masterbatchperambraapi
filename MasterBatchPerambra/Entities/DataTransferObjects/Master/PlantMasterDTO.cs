﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.DataTransferObjects
{
    
    public class PlantMasterDTO
    {
      
       
        public int Plant_Id { get; set; }
       public int Plant_Code { get; set; }
              
       public string Plant_Name { get; set; }
        public string Address { get; set; }
        public Boolean Status { get; set; }
        public DateTime Created_Date { get; set; }
        public string Created_By { get; set; }
        public DateTime Modified_Date { get; set; }
        public string Modified_By { get; set; }
        public int Company_Id { get; set; }
      



    }
}
