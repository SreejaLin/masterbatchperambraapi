﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace Entities.DataTransferObjects
{

    public class BNB_MasterDTO
    {

   
 
        public int BNB_ID { get; set; }
        
    
        public string BNB_Name { get; set; }


        public string Plant_id { get; set; }
        public Boolean BNB_Status { get; set; }

        public PlantMaster PlantMaster { get; set; }
        public string Plant_Name { get; set; }

    }
}
