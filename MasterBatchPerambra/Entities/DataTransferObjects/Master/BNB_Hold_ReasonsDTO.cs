﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.DataTransferObjects
{
    public class BNB_Hold_ReasonsDTO
    {
     
        public int Hold_reason_id { get; set; }
 
        public string Hold_reason { get; set; }
        public Boolean Status { get; set; }

    }
}
