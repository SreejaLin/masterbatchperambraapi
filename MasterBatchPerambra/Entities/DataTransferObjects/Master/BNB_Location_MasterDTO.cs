﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Entities.Models;


namespace Entities.DataTransferObjects
{
    
    public  class BNB_Location_MasterDTO
    {
    
       
        public int BNB_Location_id  { get; set; }

       
        public string BNB_Location_Name { get; set; }

        public string CompoundCategory{ get; set; } //'Master,Final,Outside,Partskid
        public Boolean BNB_Location_Status  { get; set; }

        public int Plant_Id { get; set; }

        public PlantMaster PlantMaster { get; set; }
        public string Plant_Name { get; set; }
        public Boolean Status { get; set; }
    }
}
