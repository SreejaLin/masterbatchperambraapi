﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.DataTransferObjects
{

    public class BNB_SupplierDTO
    {

        public int Supplier_Id { get; set; }

        [Required(ErrorMessage = "EmpId is required")]


        public string SupplierName { get; set; }

    }
}
