﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.Models
{

    public class BNB_MaterialMappingDTO
    {
       public int Mapping_ID { get; set; }
        public string Plant_Code { get; set; }
        public string Machine_Center { get; set; }
       public string Scheduled_Material { get; set; }
       public string Input_Material { get; set; }
       

    }
}
