﻿using System;
using Entities.Models;


namespace Entities.DataTransferObjects
{
  
    public class BNB_Compound_MasterDTO
    {
   
        public int Compound_ID { get; set; }


        public string Compound_Code { get; set; }

        public int Plant_Id { get; set; }
        public Boolean Status { get; set; }
        
         public DateTime Entry_Date { get; set; }

        public int Compound_TypeId { get; set; }
        public int Compound_AgingHours { get; set; }
        public int Compound_UseByDays { get; set; }
        public BNBCompoundTypeMaster BNBCompoundTypeMaster { get; set; }
        public PlantMaster PlantMaster { get; set; }
        public string Compound_typeName { get; set; }

        public string Plant_Name { get; set; }
        // public virtual BNB_CompoundType_Master BNB_CompoundType_Master { get; set; }



    }
}
