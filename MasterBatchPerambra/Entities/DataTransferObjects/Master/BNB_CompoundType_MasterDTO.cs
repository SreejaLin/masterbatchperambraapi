﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.DataTransferObjects
{
   
    public class BNB_CompoundType_MasterDTO
    {


        public int Compound_TypeId { get; set; }

        public string Compound_TypeName { get; set; }
        public Boolean Status { get; set; }

    }
}
