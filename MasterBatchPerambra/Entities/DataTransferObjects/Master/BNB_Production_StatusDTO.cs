﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Entities.DataTransferObjects
{
  
    public class BNB_Production_StatusDTO
    {
       
        public int Production_Status_Id   { get; set; }
     
        public string Prod_Status_Name  { get; set; }
        public Boolean Status  { get; set; }


    }
}
