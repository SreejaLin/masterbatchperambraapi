﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.DataTransferObjects
{

    public class BNB_UsersDTO
    {

        public int User_Id { get; set; }

        public int EmpId { get; set; }
        public string LoginId  { get; set; }
        public string Password  { get; set; }

    }
}
