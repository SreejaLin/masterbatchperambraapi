﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.DataTransferObjects
{

    public class BNB_ConsumptionDTO
    {

        public string ForkliftOpt_Code { get; set; }

        public int bnB_Prod_Id { get; set; }
        public string consumptionBnb  { get; set; }


    }
}
