﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
  public class BNB_LabUpdatedDTO
    {

        public int BNB_Prod_Id { get; set; }

     
        public string Plant_Code { get; set; }

        public string Prod_Barcode { get; set; }

        public string Banbury { get; set; }

        public DateTime Shift_Date { get; set; }
        public string Shift { get; set; }  
        public string Compound_Code { get; set; }

        public string Specail_Trails { get; set; }

       
        public int Skid_NO { get; set; }
    
        public string Batch_No { get; set; }

        public int No_OF_Batches { get; set; }
      
        public string Location { get; set; }


       
        public string Operator_Code { get; set; }

        
        public string Trucker_Code { get; set; }
        public string Prod_Status { get; set; }
        public string Prod_Remarks { get; set; }

        public string Lab_EmpCode { get; set; }
        public string Lab_Status { get; set; }
        public string Lab_Remarks { get; set; }
        public string Tech_Status { get; set; }
        public string Tech_Remarks { get; set; }
        public string Tech_EmpCode { get; set; }
        public DateTime BNB_ProdTime { get; set; }
        public DateTime Ageing_Time { get; set; }
        public DateTime Use_Before { get; set; }

        public DateTime Last_TagPrinting_Time { get; set; }

        public string Consumtion_Status { get; set; }
        public DateTime Consumption_Time { get; set; }
        public string Consumption_BNB { get; set; }
        public string ForkliftOpt_Code { get; set; }
        public int Ageing_Status { get; set; }
        public DateTime Created_Date { get; set; }
        public string Created_By { get; set; }
        public DateTime Modified_DateTime { get; set; }
        public string Modified_By { get; set; }

        public string Module_Name { get; set; }
        public string Hold_reason { get; set; }

        public int Hold_reason_id { get; set; }
        public string bNBShiftDate { get; set; }

        public string BatchI { get; set; }
        public string BatchII { get; set; }

        public string Vendor { get; set; }
        public string RMS_Crew { get; set; }
        public List<BNBProductionEntry> BNBProductionEntry { get; set; }

    }
}
