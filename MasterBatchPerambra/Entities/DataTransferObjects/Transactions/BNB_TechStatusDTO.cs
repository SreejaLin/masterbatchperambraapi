﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
  public  class BNB_TechStatusDTO
    {
        public int BNB_Prod_Id { get; set; }
        public string Tech_EmpCode { get; set; }
        public string Tech_Status { get; set; }
        public string Tech_Remarks { get; set; }
        public string Prod_Barcode { get; set; }

        public DateTime Use_Before { get; set; }


        public string RelaxedTime { get; set; }
    }
}
