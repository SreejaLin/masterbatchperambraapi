﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Entities.Models;

namespace Entities.DataTransferObjects
{
  
   
    public class BNB_RmsDTO
    {
        public int BNB_Prod_Id { get; set; }

        public string Plant_Code { get; set; }
        public string Prod_Barcode { get; set; }

       
        public DateTime Shift_Date { get; set; }
        public string Shift { get; set; }
        [Required(ErrorMessage = "Compound_Code is required")]
        public string Compound_Code { get; set; }

        public string Specail_Trails { get; set; }

        [Required(ErrorMessage = "SKID_NO is required")]
        public int Skid_NO { get; set; }
        [Required(ErrorMessage = "BATCH_NO is required")]
        public string Batch_No { get; set; }

       
        [Required(ErrorMessage = "Location is required")]
        public string Location { get; set; }


        public int No_OF_Batches { get; set; }
        public DateTime BNB_ProdTime { get; set; }
        public DateTime Ageing_Time { get; set; }
        public DateTime Use_Before { get; set; }

        public DateTime Last_TagPrinting_Time { get; set; }

        public string Consumtion_Status { get; set; }
        public DateTime Consumption_Time { get; set; }
        public string Consumption_BNB { get; set; }

        public int Ageing_Status { get; set; }
        public DateTime Created_Date { get; set; }
        public string Created_By { get; set; }
        public DateTime Modified_DateTime { get; set; }
        public string Modified_By { get; set; }

        public string bNBShiftDate { get; set; }

        public string BatchI { get; set; }
        public string BatchII { get; set; }

        public string Vendor { get; set; }
        public string RMS_Crew { get; set; }
        public string Prod_Status{ get; set; }
        public string Module_Name { get; set; }
        public string LabStatus { get; set; }
        public string Banbury { get; set; }
    }
}
