﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
  public  class BNB_LabStatusDTO
    {
        public int BNB_Prod_Id { get; set; }
        public string Lab_EmpCode { get; set; }
        public string Lab_Status { get; set; }
        public string Lab_Remarks { get; set; }
        public string Prod_Barcode { get; set; }
    }
}
