﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Entities.Models;

namespace Entities.DataTransferObjects
{
  
   
    public class BNB_LogStatusDTO
    {
        public int Log_Id { get; set; }
        public int BNB_Prod_id { get; set; }
        public string Plant_code { get; set; }
        public string Prod_Barcode { get; set; }
        public DateTime ActionTime { get; set; }
        public string UserGroup { get; set; }
        public string User { get; set; }
        public string UserRemarks { get; set; }
        public string Actions { get; set; }
        public Boolean HostName { get; set; }
        public BNBProductionEntry BNBProductionEntry { get; set; }

        public string Shift_Date { get; set; }

        public string Shift { get; set; }

        public string Compound_Code { get; set; }
        public string Skid_NO { get; set; }
        public string Batch_No { get; set; }

        public string Location { get; set; }
        public string Prod_Status { get; set; }

        public string Prod_Remarks { get; set; }

        public string Lab_Status { get; set; }
        public string Lab_Remarks { get; set; }

        public string Banbury { get; set; }

    }
}
