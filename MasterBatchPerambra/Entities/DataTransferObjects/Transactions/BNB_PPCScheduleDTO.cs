﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class BNB_PPCScheduleDTO
    {
        public string Comp_Code { get; set; }

        public int Sch_value { get; set; }
        public string Sch_Remarks { get; set; }

        public string RPM { get; set; }
        public string Date { get; set; }
        public string shift { get; set; }

        public string Mapped_Comp_Code { get; set; }


    }
}
