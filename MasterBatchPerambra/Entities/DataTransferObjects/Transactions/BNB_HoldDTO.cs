﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
 public class BNB_HoldDTO
    {
       
        public int Hold_id { get; set; }
        public int Hold_Reason_Id { get; set; }
        public int Prod_Id { get; set; }
        public DateTime Hold_Datetime { get; set; }
        public string HoldBy_Authority { get; set; }
        public string HoldBy_EmpId { get; set; }
        public string Shift { get; set; }
        public string NotOk_LabRemarks { get; set; }
        public Boolean Status { get; set; }
    }
}
