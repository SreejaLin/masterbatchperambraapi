﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class plantcode1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BNB_Master_PlantMaster_Plant_Id",
                table: "BNB_Master");

            migrationBuilder.DropIndex(
                name: "IX_BNB_Master_Plant_Id",
                table: "BNB_Master");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_BNB_Master_Plant_Id",
                table: "BNB_Master",
                column: "Plant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BNB_Master_PlantMaster_Plant_Id",
                table: "BNB_Master",
                column: "Plant_Id",
                principalTable: "PlantMaster",
                principalColumn: "Plant_Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
