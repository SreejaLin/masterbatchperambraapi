﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class vendorproducentry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RMS_Crew",
                table: "BNB_Production_Entry",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Vendor",
                table: "BNB_Production_Entry",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RMS_Crew",
                table: "BNB_Production_Entry");

            migrationBuilder.DropColumn(
                name: "Vendor",
                table: "BNB_Production_Entry");
        }
    }
}
