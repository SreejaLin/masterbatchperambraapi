﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class mappingtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BNB_Compound_Master_BNB_CompoundType_Master_Compound_TypeId",
                table: "BNB_Compound_Master");

            migrationBuilder.DropForeignKey(
                name: "FK_BNB_Location_Master_PlantMaster_Plant_Id",
                table: "BNB_Location_Master");

            migrationBuilder.DropForeignKey(
                name: "FK_BNB_Master_PlantMaster_Plant_Id",
                table: "BNB_Master");

            migrationBuilder.DropIndex(
                name: "IX_BNB_Master_Plant_Id",
                table: "BNB_Master");

            migrationBuilder.DropIndex(
                name: "IX_BNB_Location_Master_Plant_Id",
                table: "BNB_Location_Master");

            migrationBuilder.DropIndex(
                name: "IX_BNB_Compound_Master_Compound_TypeId",
                table: "BNB_Compound_Master");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_BNB_Master_Plant_Id",
                table: "BNB_Master",
                column: "Plant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Location_Master_Plant_Id",
                table: "BNB_Location_Master",
                column: "Plant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Compound_Master_Compound_TypeId",
                table: "BNB_Compound_Master",
                column: "Compound_TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_BNB_Compound_Master_BNB_CompoundType_Master_Compound_TypeId",
                table: "BNB_Compound_Master",
                column: "Compound_TypeId",
                principalTable: "BNB_CompoundType_Master",
                principalColumn: "Compound_TypeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BNB_Location_Master_PlantMaster_Plant_Id",
                table: "BNB_Location_Master",
                column: "Plant_Id",
                principalTable: "PlantMaster",
                principalColumn: "Plant_Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BNB_Master_PlantMaster_Plant_Id",
                table: "BNB_Master",
                column: "Plant_Id",
                principalTable: "PlantMaster",
                principalColumn: "Plant_Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
