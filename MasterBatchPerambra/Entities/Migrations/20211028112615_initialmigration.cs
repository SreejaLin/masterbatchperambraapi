﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class initialmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BNB_Compound_Aging_Relaxation",
                columns: table => new
                {
                    Aging_relaxation_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Compound_Aging_Relaxation", x => x.Aging_relaxation_id);
                });

            migrationBuilder.CreateTable(
                name: "BNB_CompoundType_Master",
                columns: table => new
                {
                    Compound_TypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Compound_TypeName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_CompoundType_Master", x => x.Compound_TypeId);
                });

            migrationBuilder.CreateTable(
                name: "BNB_Hold_Reasons",
                columns: table => new
                {
                    Hold_reason_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Hold_reason = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Hold_Reasons", x => x.Hold_reason_id);
                });

            migrationBuilder.CreateTable(
                name: "BNB_Production_Entry",
                columns: table => new
                {
                    BNB_Prod_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Plant_Code = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Prod_Barcode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Banbury = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Shift_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Shift = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Compound_Code = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Specail_Trails = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Skid_NO = table.Column<int>(type: "int", nullable: false),
                    Batch_No = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    No_OF_Batches = table.Column<int>(type: "int", nullable: false),
                    Location = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Operator_Code = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Trucker_Code = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Prod_Status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Prod_Remarks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Lab_EmpCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Tech_Status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Tech_Remarks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Tech_EmpCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BNB_ProdTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Ageing_Time = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Use_Before = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Last_TagPrinting_Time = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Consumtion_Status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Consumption_Time = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Consumption_BNB = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ForkliftOpt_Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ageing_Status = table.Column<int>(type: "int", nullable: false),
                    Created_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Created_By = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Modified_DateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_By = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Production_Entry", x => x.BNB_Prod_Id);
                });

            migrationBuilder.CreateTable(
                name: "BNB_Production_Status",
                columns: table => new
                {
                    Production_Status_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Prod_Status_Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Production_Status", x => x.Production_Status_Id);
                });

            migrationBuilder.CreateTable(
                name: "BNB_Tag_Generation",
                columns: table => new
                {
                    TagId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TagDetails = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location_Id = table.Column<int>(type: "int", nullable: false),
                    Printer_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Printer_Ip = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Printed_DateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Module_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    User_Id = table.Column<int>(type: "int", nullable: true),
                    Print_Status = table.Column<int>(type: "int", nullable: false),
                    Prod_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Tag_Generation", x => x.TagId);
                });

            migrationBuilder.CreateTable(
                name: "BNB_Users",
                columns: table => new
                {
                    User_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmpId = table.Column<int>(type: "int", nullable: false),
                    LoginId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Users", x => x.User_Id);
                });

            migrationBuilder.CreateTable(
                name: "PlantMaster",
                columns: table => new
                {
                    Plant_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Plant_Code = table.Column<int>(type: "int", nullable: false),
                    Plant_Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Address = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    Created_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Created_By = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Modified_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_By = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Company_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantMaster", x => x.Plant_Id);
                });

            migrationBuilder.CreateTable(
                name: "BNB_Hold",
                columns: table => new
                {
                    Hold_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Prod_Id = table.Column<int>(type: "int", nullable: false),
                    Hold_Datetime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    HoldBy_Authority = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HoldBy_EmpId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Shift = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NotOk_LabRemarks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    Hold_Reason_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Hold", x => x.Hold_id);
                    table.ForeignKey(
                        name: "FK_BNB_Hold_BNB_Hold_Reasons_Hold_Reason_Id",
                        column: x => x.Hold_Reason_Id,
                        principalTable: "BNB_Hold_Reasons",
                        principalColumn: "Hold_reason_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BNB_Compound_Master",
                columns: table => new
                {
                    Compound_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Entry_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Compound_Code = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Plant_Id = table.Column<int>(type: "int", nullable: false),
                    Compound_AgingHours = table.Column<int>(type: "int", nullable: false),
                    Compound_UseByDays = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    Compound_TypeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Compound_Master", x => x.Compound_ID);
                    table.ForeignKey(
                        name: "FK_BNB_Compound_Master_BNB_CompoundType_Master_Compound_TypeId",
                        column: x => x.Compound_TypeId,
                        principalTable: "BNB_CompoundType_Master",
                        principalColumn: "Compound_TypeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BNB_Compound_Master_PlantMaster_Plant_Id",
                        column: x => x.Plant_Id,
                        principalTable: "PlantMaster",
                        principalColumn: "Plant_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BNB_Location_Master",
                columns: table => new
                {
                    BNB_Location_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BNB_Location_Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    CompoundCategory = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BNB_Location_Status = table.Column<bool>(type: "bit", nullable: false),
                    Plant_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Location_Master", x => x.BNB_Location_id);
                    table.ForeignKey(
                        name: "FK_BNB_Location_Master_PlantMaster_Plant_Id",
                        column: x => x.Plant_Id,
                        principalTable: "PlantMaster",
                        principalColumn: "Plant_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BNB_Master",
                columns: table => new
                {
                    BNB_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BNB_Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Plant_Id = table.Column<int>(type: "int", nullable: false),
                    BNB_Status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Master", x => x.BNB_ID);
                    table.ForeignKey(
                        name: "FK_BNB_Master_PlantMaster_Plant_Id",
                        column: x => x.Plant_Id,
                        principalTable: "PlantMaster",
                        principalColumn: "Plant_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Compound_Master_Compound_Code",
                table: "BNB_Compound_Master",
                column: "Compound_Code",
                unique: true,
                filter: "[Compound_Code] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Compound_Master_Compound_TypeId",
                table: "BNB_Compound_Master",
                column: "Compound_TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Compound_Master_Plant_Id",
                table: "BNB_Compound_Master",
                column: "Plant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_BNB_CompoundType_Master_Compound_TypeName",
                table: "BNB_CompoundType_Master",
                column: "Compound_TypeName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Hold_Hold_Reason_Id",
                table: "BNB_Hold",
                column: "Hold_Reason_Id");

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Hold_Reasons_Hold_reason",
                table: "BNB_Hold_Reasons",
                column: "Hold_reason",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Location_Master_BNB_Location_Name",
                table: "BNB_Location_Master",
                column: "BNB_Location_Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Location_Master_Plant_Id",
                table: "BNB_Location_Master",
                column: "Plant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Master_BNB_Name",
                table: "BNB_Master",
                column: "BNB_Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Master_Plant_Id",
                table: "BNB_Master",
                column: "Plant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Production_Status_Prod_Status_Name",
                table: "BNB_Production_Status",
                column: "Prod_Status_Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BNB_Compound_Aging_Relaxation");

            migrationBuilder.DropTable(
                name: "BNB_Compound_Master");

            migrationBuilder.DropTable(
                name: "BNB_Hold");

            migrationBuilder.DropTable(
                name: "BNB_Location_Master");

            migrationBuilder.DropTable(
                name: "BNB_Master");

            migrationBuilder.DropTable(
                name: "BNB_Production_Entry");

            migrationBuilder.DropTable(
                name: "BNB_Production_Status");

            migrationBuilder.DropTable(
                name: "BNB_Tag_Generation");

            migrationBuilder.DropTable(
                name: "BNB_Users");

            migrationBuilder.DropTable(
                name: "BNB_CompoundType_Master");

            migrationBuilder.DropTable(
                name: "BNB_Hold_Reasons");

            migrationBuilder.DropTable(
                name: "PlantMaster");
        }
    }
}
