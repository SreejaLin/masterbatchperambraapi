﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Logstatusfk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_BNB_LogStatus_BNB_Prod_id",
                table: "BNB_LogStatus",
                column: "BNB_Prod_id");

            migrationBuilder.AddForeignKey(
                name: "FK_BNB_LogStatus_BNB_Production_Entry_BNB_Prod_id",
                table: "BNB_LogStatus",
                column: "BNB_Prod_id",
                principalTable: "BNB_Production_Entry",
                principalColumn: "BNB_Prod_Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BNB_LogStatus_BNB_Production_Entry_BNB_Prod_id",
                table: "BNB_LogStatus");

            migrationBuilder.DropIndex(
                name: "IX_BNB_LogStatus_BNB_Prod_id",
                table: "BNB_LogStatus");
        }
    }
}
