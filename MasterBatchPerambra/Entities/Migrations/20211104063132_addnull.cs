﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class addnull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BNB_Compound_Master_BNB_CompoundType_Master_Compound_TypeId",
                table: "BNB_Compound_Master");

            migrationBuilder.AlterColumn<int>(
                name: "Compound_TypeId",
                table: "BNB_Compound_Master",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_BNB_Compound_Master_BNB_CompoundType_Master_Compound_TypeId",
                table: "BNB_Compound_Master",
                column: "Compound_TypeId",
                principalTable: "BNB_CompoundType_Master",
                principalColumn: "Compound_TypeId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BNB_Compound_Master_BNB_CompoundType_Master_Compound_TypeId",
                table: "BNB_Compound_Master");

            migrationBuilder.AlterColumn<int>(
                name: "Compound_TypeId",
                table: "BNB_Compound_Master",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BNB_Compound_Master_BNB_CompoundType_Master_Compound_TypeId",
                table: "BNB_Compound_Master",
                column: "Compound_TypeId",
                principalTable: "BNB_CompoundType_Master",
                principalColumn: "Compound_TypeId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
