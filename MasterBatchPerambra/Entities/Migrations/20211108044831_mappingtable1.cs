﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class mappingtable1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BNB_Material_Mapping",
                columns: table => new
                {
                    Mapping_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Plant_Id = table.Column<int>(type: "int", nullable: false),
                    Machine_Center = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Scheduled_Material = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Input_Material = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BNB_Material_Mapping", x => x.Mapping_ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BNB_Material_Mapping");
        }
    }
}
