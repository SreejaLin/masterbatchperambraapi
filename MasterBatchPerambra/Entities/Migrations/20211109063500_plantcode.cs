﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class plantcode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Plant_Id",
                table: "BNB_Material_Mapping");

            migrationBuilder.AddColumn<string>(
                name: "Plant_Code",
                table: "BNB_Material_Mapping",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BNB_Master_Plant_Id",
                table: "BNB_Master",
                column: "Plant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BNB_Master_PlantMaster_Plant_Id",
                table: "BNB_Master",
                column: "Plant_Id",
                principalTable: "PlantMaster",
                principalColumn: "Plant_Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BNB_Master_PlantMaster_Plant_Id",
                table: "BNB_Master");

            migrationBuilder.DropIndex(
                name: "IX_BNB_Master_Plant_Id",
                table: "BNB_Master");

            migrationBuilder.DropColumn(
                name: "Plant_Code",
                table: "BNB_Material_Mapping");

            migrationBuilder.AddColumn<int>(
                name: "Plant_Id",
                table: "BNB_Material_Mapping",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
