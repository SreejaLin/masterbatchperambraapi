﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class logstatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserGroup",
                table: "BNB_Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Lab_Remarks",
                table: "BNB_Production_Entry",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Lab_Status",
                table: "BNB_Production_Entry",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserGroup",
                table: "BNB_Users");

            migrationBuilder.DropColumn(
                name: "Lab_Remarks",
                table: "BNB_Production_Entry");

            migrationBuilder.DropColumn(
                name: "Lab_Status",
                table: "BNB_Production_Entry");
        }
    }
}
