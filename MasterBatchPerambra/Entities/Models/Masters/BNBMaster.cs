﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    [Table("BNB_Master")]
    [Index(nameof(BNB_Name), IsUnique = true)]
    public class BNBMaster
    {

   
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BNB_ID { get; set; }
        
        [Required(ErrorMessage = "BNB_Name is required")]
        public string BNB_Name { get; set; }


        [Required(ErrorMessage = "Plant is required")]
        [ForeignKey("PlantMaster")]
        public int Plant_Id { get; set; }
       // public PlantMaster PlantMaster { get; set; }
        public Boolean BNB_Status { get; set; }
  


    }
}
