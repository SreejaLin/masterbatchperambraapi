﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("BNB_Hold_Reasons")]
    [Index(nameof(Hold_reason), IsUnique = true)]
    public class BNBHoldReasons
    {
     
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Hold_reason_id { get; set; }
        [Required(ErrorMessage = "Hold_reason is required")]
        [StringLength(100, ErrorMessage = "Hold_reason can't be longer than 200 characters")]
        public string Hold_reason { get; set; }
        public Boolean Status { get; set; }

    }
}
