﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.Models
{
    [Table("BNB_Production_Status")]
    [Index(nameof(Prod_Status_Name), IsUnique = true)]
    public class BNBProductionStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Production_Status_Id   { get; set; }
        [Required(ErrorMessage = "Production Status Name is required")]
        [StringLength(100, ErrorMessage = "Prod_Status_Name can't be longer than 100 characters")]
        public string Prod_Status_Name  { get; set; }
        public Boolean Status  { get; set; }


    }
}
