﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.Models
{
    [Table("BNB_Supplier")]
    public class BNBSupplier
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Supplier_Id { get; set; }

        [Required(ErrorMessage = "EmpId is required")]
       

        public string SupplierName { get; set; }

    }
}
