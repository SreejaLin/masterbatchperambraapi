﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;


namespace Entities.Models
{
    [Table("BNB_Location_Master")]
    [Index(nameof(BNB_Location_Name), IsUnique = true)]
    public  class BNBLocationMaster
    {
    
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BNB_Location_id  { get; set; }

        [Required(ErrorMessage = "BNB_Location_Name is required")]
        public string BNB_Location_Name { get; set; }

        public string CompoundCategory{ get; set; } //'Master,Final,Outside,Partskid
        public Boolean BNB_Location_Status  { get; set; }

        [ForeignKey("PlantMaster")]
        public int Plant_Id { get; set; }
      //  public PlantMaster PlantMaster { get; set; }

        public Boolean Status { get; set; }
    }
}
