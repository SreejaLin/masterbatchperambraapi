﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.Models
{
    [Table("BNB_Users")]
    public class BNBUsers
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int User_Id { get; set; }

        [Required(ErrorMessage = "EmpId is required")]
        public int EmpId { get; set; }
        public string LoginId  { get; set; }
        public string Password  { get; set; }

        public string UserGroup { get; set; }

    }
}
