﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.Models
{
    [Table("BNB_Compound_Master")]
    [Index(nameof(Compound_Code), IsUnique = true)]
    public class BNBCompoundMaster
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Compound_ID { get; set; }

        [Required(ErrorMessage = "Compound_Code is required")]
       public DateTime Entry_Date { get; set; }
        public string Compound_Code { get; set; }
      
        [ForeignKey("PlantMaster")]
        public int Plant_Id { get; set; }
        public PlantMaster PlantMaster { get; set; }
        public int Compound_AgingHours { get; set; }
        public int Compound_UseByDays { get; set; }
        public Boolean Status { get; set; }
        
        [ForeignKey("BNBCompoundTypeMaster")]
        public int? Compound_TypeId { get; set; }
        
   //     public BNBCompoundTypeMaster BNBCompoundTypeMaster { get; set; }
       // public virtual BNB_CompoundType_Master BNB_CompoundType_Master { get; set; }

       
    }
}
