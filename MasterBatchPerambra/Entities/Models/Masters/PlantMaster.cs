﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.Models
{
    [Table("PlantMaster")]
    public class PlantMaster
    {
      
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Plant_Id { get; set; }
        [Required(ErrorMessage = "Plant_Code is required")]

        public int Plant_Code { get; set; }

        [Required(ErrorMessage = "Plant Name is required")]
        [StringLength(100, ErrorMessage = "Plant Name can't be longer than 100 characters")]
        public string Plant_Name { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [StringLength(500, ErrorMessage = "Address can't be longer than 500 characters")]
        public string Address { get; set; }
        public Boolean Status { get; set; }
        public DateTime Created_Date { get; set; }
        public string Created_By { get; set; }
        public DateTime Modified_Date { get; set; }
        public string Modified_By { get; set; }


        public int Company_Id { get; set; }
        //[ForeignKey("Company_Id")]
        ////public CompanyMaster CompanyMaster { get; set; }

       // public virtual CompanyMaster CompanyMaster { get; set; }





    }
}
