﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.Models
{
    [Table("BNB_CompoundType_Master")]
    [Index(nameof(Compound_TypeName), IsUnique = true)]
    public class BNBCompoundTypeMaster
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Compound_TypeId { get; set; }

        [Required(ErrorMessage = "Compound Type Name is required")]
        [StringLength(50, ErrorMessage = "Plant Name can't be longer than 50 characters")]
        public string Compound_TypeName { get; set; }
        public Boolean Status { get; set; }

    }
}
