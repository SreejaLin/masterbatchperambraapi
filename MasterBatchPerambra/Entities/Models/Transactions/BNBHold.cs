﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("BNB_Hold")]
  
    public class BNBHold
    {
     
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Hold_id { get; set; }
        //[Required(ErrorMessage = "Hold_Reason_Id is required")]
        //[StringLength(100, ErrorMessage = "Hold_Reason_Id can't be longer than 200 characters")]
        //public int Hold_Reason_Id   { get; set; }
        public int Prod_Id { get; set; }
        public DateTime Hold_Datetime { get; set; }
        public string HoldBy_Authority { get; set; }
        public string HoldBy_EmpId { get; set; }
        public string Shift { get; set; }
        public string NotOk_LabRemarks { get; set; }
        public Boolean Status { get; set; }
        [ForeignKey("BNBHoldReasons")]
        public int Hold_Reason_Id { get; set; }
        public BNBHoldReasons BNBHoldReasons { get; set; }

    }
}
