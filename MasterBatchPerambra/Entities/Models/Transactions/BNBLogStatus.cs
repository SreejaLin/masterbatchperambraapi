﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Entities.Models
{
    [Table("BNB_LogStatus")]
   
    public class BNBLogStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Log_Id { get; set; }
        [ForeignKey("BNBProductionEntry")]
        

        public int BNB_Prod_id { get; set; }
        public BNBProductionEntry BNBProductionEntry { get; set; }
        public string Plant_code { get; set; }
        public string Prod_Barcode { get; set; }
        public DateTime ActionTime { get; set; }
        public string UserGroup { get; set; }
        public string User { get; set; }
        public string UserRemarks { get; set; }
        public string Actions { get; set; }
        public Boolean HostName { get; set; }

        //public string Banbury { get; set; }

    }
}
