﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    [Table("BNB_Tag_Generation")]

    public class BNBTagGeneration
    {
        [Key]
        public int TagId { get; set; }
        public string TagDetails { get; set; }
        public int Location_Id { get; set; }
        public string Printer_Name { get; set; }
        public string Printer_Ip { get; set; }
        public DateTime Printed_DateTime { get; set; }
        public string? Module_Name { get; set; }
        public int? User_Id { get; set; }
        public int Print_Status { get; set; }

        // [ForeignKey(nameof(FGS_FIFO_Daily_Receipt_Transaction))] 
      //  public int? InvTranId { get; set; }
        //  public FGS_FIFO_Daily_Receipt_Transaction FGS_FIFO_Daily_Receipt_Transaction { get; set; }
        public int Prod_Id { get; set; }

    }
}
