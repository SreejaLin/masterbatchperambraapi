﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBMasterRepository : RepositoryBase<BNBMaster>, IBNBMasterRepository
    {
        public BNBMasterRepository(RepositoryContext repositoryContext)
      : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBMaster>> GetAllbanburys()


        {

            return await FindAll()
               // .Include(Y => Y.PlantMaster)
                .ToListAsync();

        }

        public void Createbanbury(BNBMaster banbury)

        {

            Create(banbury);
        }
        public void Deletebanbury(BNBMaster banbury)
        {
            Delete(banbury);
        }

        public BNBMaster GetbanburyById(Int64 banbury_Id)
        {
            return FindByCondition(BNB_Master => BNB_Master.BNB_ID == banbury_Id).FirstOrDefault();
        }

        public BNBMaster GetbanburyByName(string banbury_Name)
        {
            return FindByCondition(BNB_Master => BNB_Master.BNB_Name == banbury_Name).FirstOrDefault();
        }


        public void Updatebanbury(BNBMaster banbury)
        {


            Update(banbury);
        }


    }
}
