﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBCompoundTypeMasterRepository : RepositoryBase<BNBCompoundTypeMaster>, IBNBCompoundTypeMasterRepository
    {
        public BNBCompoundTypeMasterRepository(RepositoryContext repositoryContext)
  : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBCompoundTypeMaster>> GetAllCompoundType()


        {

            return await FindAll().ToListAsync();

        }

        public void CreateCompoundType(BNBCompoundTypeMaster CompoundType)

        {

            Create(CompoundType);
        }
        public void DeleteCompoundType(BNBCompoundTypeMaster CompoundType)
        {
            Delete(CompoundType);
        }

        public BNBCompoundTypeMaster GetCompoundTypeById(Int64 CompoundType_Id)
        {
            return FindByCondition(BNB_CompoundType_Master => BNB_CompoundType_Master.Compound_TypeId == CompoundType_Id).FirstOrDefault();
        }

        public BNBCompoundTypeMaster GetCompoundTypeByNamebanbury(Int64 bnb, string CompoundType_Name)
        {
            return FindByCondition(BNB_CompoundType_Master => BNB_CompoundType_Master.Compound_TypeName == CompoundType_Name).FirstOrDefault();
        }


        public void UpdateCompoundType(BNBCompoundTypeMaster CompoundType)
        {


            Update(CompoundType);
        }
    }
}
