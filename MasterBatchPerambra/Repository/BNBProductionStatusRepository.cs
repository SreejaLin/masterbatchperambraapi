﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBProductionStatusRepository : RepositoryBase<BNBProductionStatus>, IBNBProductionStatusRepository
    {
        public BNBProductionStatusRepository(RepositoryContext repositoryContext)
       : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBProductionStatus>> GetAllproduction_Statuss()


        {

            return await FindAll().ToListAsync();

        }

        public void Createproduction_Status(BNBProductionStatus production_Status)

        {

            Create(production_Status);
        }
        public void Deleteproduction_Status(BNBProductionStatus production_Status)
        {
            Delete(production_Status);
        }

        public BNBProductionStatus Getproduction_StatusById(Int64 status_Id)
        {
            return FindByCondition(BNB_Production_Status => BNB_Production_Status.Production_Status_Id == status_Id).FirstOrDefault();
        }

        public BNBProductionStatus Getproduction_StatusByName( string production_Status_Name)
        {
            return FindByCondition(BNB_Production_Status => BNB_Production_Status.Prod_Status_Name == production_Status_Name ).FirstOrDefault();
        }


        public void Updateproduction_Status(BNBProductionStatus production_Status)
        {


            Update(production_Status);
        }


    }
}
