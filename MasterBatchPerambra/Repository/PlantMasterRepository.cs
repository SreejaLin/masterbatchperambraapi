﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class PlantMasterRepository : RepositoryBase<PlantMaster>, IPlantMasterRepository
    {
        public PlantMasterRepository(RepositoryContext repositoryContext)
        : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<PlantMaster>> GetAllPlants()
        {
            return await FindAll().ToListAsync();
        }
        public void CreatePlant(PlantMaster plant)
        {
            Create(plant);
        }
        public void DeletePlant(PlantMaster plant)
        {
            Delete(plant);
        }
        public PlantMaster GetPlantById(Int64 plant_Id)
        {
            return FindByCondition(PlantMaster => PlantMaster.Plant_Id == plant_Id).FirstOrDefault();
        }
        public PlantMaster GetPlantByNameCompany(Int64 Company_Id, string Plant_Name)
        {
            return FindByCondition(PlantMaster => PlantMaster.Plant_Name == Plant_Name && PlantMaster.Company_Id == Company_Id).FirstOrDefault();
        }
        public void UpdatePlant(PlantMaster plant)
        {
            Update(plant);
        }

    }
}
