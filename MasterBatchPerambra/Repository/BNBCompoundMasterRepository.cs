﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBCompoundMasterRepository : RepositoryBase<BNBCompoundMaster>, IBNBCompoundMasterRepository
    {
        public BNBCompoundMasterRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBCompoundMaster>> GetAllCompound()
        {

            return await FindAll()
                //.Include(x => x.BNBCompoundTypeMaster)
               // .Include(Y => Y.PlantMaster)
                .ToListAsync();

        }

        public void CreateCompound(BNBCompoundMaster Compound)

        {

            Create(Compound);
        }
        public void DeleteCompound(BNBCompoundMaster Compound)
        {
            Delete(Compound);
        }

        public BNBCompoundMaster GetCompoundById(Int64 Compound_Id)
        {
            return FindByCondition(BNB_Compound => BNB_Compound.Compound_ID == Compound_Id).FirstOrDefault();
        }

        public BNBCompoundMaster GetCompoundByNamebanbury(Int64 bnb, string Compound_Name)
        {
            return FindByCondition(BNB_Compound => BNB_Compound.Compound_Code == Compound_Name).FirstOrDefault();
        }


        public void UpdateCompound(BNBCompoundMaster Compound)
        {


            Update(Compound);
        }
    }
}
