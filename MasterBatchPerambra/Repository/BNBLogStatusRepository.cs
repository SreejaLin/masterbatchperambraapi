﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBLogStatusRepository : RepositoryBase<BNBLogStatus>, IBNBLogStatusRepository
    {
        public BNBLogStatusRepository(RepositoryContext repositoryContext)
    : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBLogStatus>> GetAllLogStatus()


        {

            return await FindAll().Include(x=> x.BNBProductionEntry)
            .ToListAsync();

        }

        public void CreateLogStatus(BNBLogStatus LogStatus)

        {

            Create(LogStatus);
        }
        public void DeleteLogStatus(BNBLogStatus LogStatus)
        {
            Delete(LogStatus);
        }

        public BNBLogStatus GetLogStatusById(int LogStatus_Id)
        {
            return FindByCondition(BNB_LogStatus => BNB_LogStatus.Log_Id == LogStatus_Id).FirstOrDefault();
        }

       


        public void UpdateLogStatus(BNBLogStatus LogStatus)
        {


            Update(LogStatus);
        }


        public async  Task<IEnumerable<BNBLogStatus>> GetUpdatedProdEntryForLabEmp(string labemp)//, string compound)
        {
            //where action time between and user from logstatus table for current shift. 
           
            DateTime proddate = DateTime.Today;
           
            string startdate = DateTime.Today.ToShortDateString();
            string enddate = DateTime.Today.ToShortDateString();
            string shift;
            DateTime ShiftB, ShiftC, EndDayTime, NextDayStartTime, lastDay, ShiftA;
            DateTime now = DateTime.Now;
            DateTime curTime = DateTime.Now;
            ShiftA = Convert.ToDateTime(" 06:01:00 AM");
            ShiftB = Convert.ToDateTime(" 02:01:00 PM");
            ShiftC = Convert.ToDateTime(" 10:01:00 PM");
            EndDayTime = Convert.ToDateTime(" 11:59:55 PM");
            NextDayStartTime = Convert.ToDateTime(" 12:00:00 AM");
         
            if (curTime >= ShiftA && curTime < ShiftB)
            {
                startdate = startdate + " 06:00:00";
                enddate = enddate + " 13:59:00";
            }
            else if (curTime >= ShiftB && curTime < ShiftC)
            {
                startdate = startdate + " 14:00:00";
                enddate = enddate + " 21:59:00";
            }
            else if (curTime >= ShiftC && curTime <= EndDayTime)
            {
                startdate = startdate + " 22:00:00";
                enddate = Convert.ToDateTime(enddate).AddDays(1).ToShortDateString() + " 05:59:00";
            }
            else if (curTime >= NextDayStartTime && curTime < ShiftA)
            {
                startdate = Convert.ToDateTime(startdate).AddDays(-1).ToShortDateString() + " 22:00:00";
                enddate = enddate + " 05:59:00";
            }

            var bnblogstatus = await FindByCondition(BNB_logstatus => BNB_logstatus.ActionTime <= Convert.ToDateTime(enddate) && BNB_logstatus.ActionTime >= Convert.ToDateTime(startdate) && BNB_logstatus.User == labemp && BNB_logstatus.UserGroup == "Lab")
            .Include(pr => pr.BNBProductionEntry).OrderByDescending(x=>x.BNB_Prod_id).ToListAsync();

       

            return bnblogstatus;
        }

      


    }
}
