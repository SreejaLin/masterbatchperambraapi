﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBSupplierRepository : RepositoryBase<BNBSupplier>, IBNBSupplierRepository
    {
        public BNBSupplierRepository(RepositoryContext repositoryContext)
       : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBSupplier>> GetAllSupplier()


        {

            return await FindAll().ToListAsync();

        }

        public void CreateSupplier(BNBSupplier Supplier)

        {

            Create(Supplier);
        }
        public void DeleteSupplier(BNBSupplier Supplier)
        {
            Delete(Supplier);
        }

        public BNBSupplier GetSupplierById(Int64 Supplier_Id)
        {
            return FindByCondition(BNB_Supplier => BNB_Supplier.Supplier_Id == Supplier_Id).FirstOrDefault();
        }

      

        public void UpdateSupplier(BNBSupplier Supplier)
        {


            Update(Supplier);
        }

    }
}
