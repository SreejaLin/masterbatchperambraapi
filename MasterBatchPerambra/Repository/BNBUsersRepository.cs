﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBUsersRepository : RepositoryBase<BNBUsers>, IBNBUsersRepository
    {
        public BNBUsersRepository(RepositoryContext repositoryContext)
       : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBUsers>> GetAllUsers()


        {

            return await FindAll().ToListAsync();

        }

        public void CreateUser(BNBUsers User)

        {

            Create(User);
        }
        public void DeleteUser(BNBUsers User)
        {
            Delete(User);
        }

        public BNBUsers GetUserById(Int64 User_Id)
        {
            return FindByCondition(BNB_Users => BNB_Users.User_Id == User_Id).FirstOrDefault();
        }

        public BNBUsers GetUserByNameCompany(Int64 Company_Id, int User_Name)
        {
            return FindByCondition(BNB_Users => BNB_Users.EmpId == User_Name ).FirstOrDefault();
        }


        public void UpdateUser(BNBUsers User)
        {


            Update(User);
        }

    }
}
