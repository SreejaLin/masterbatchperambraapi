﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBHoldRepository : RepositoryBase<BNBHold>, IBNBHoldRepository
    {
        public BNBHoldRepository(RepositoryContext repositoryContext)
    : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBHold>> GetAllHold()


        {

            return await FindAll().ToListAsync();

        }

        public void CreateHold(BNBHold Hold)

        {

            Create(Hold);
        }
        public void DeleteHold(BNBHold Hold)
        {
            Delete(Hold);
        }

        public BNBHold GetHoldById(int Hold_Id)
        {
            return FindByCondition(BNB_Hold => BNB_Hold.Hold_Reason_Id == Hold_Id).FirstOrDefault();
        }

       


        public void UpdateHold(BNBHold Hold)
        {


            Update(Hold);
        }
    }
}
