﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces;
using Entities;
using Microsoft.EntityFrameworkCore;
namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repoContext;



        private IBNBCompoundAgingRelaxationRepository _CompoundAgingRelaxation;
        private IBNBCompoundMasterRepository _CompoundMaster;

        private IBNBCompoundTypeMasterRepository _CompoundTypeMaster;
        private IBNBHoldRepository _Hold;
        private IBNBTagGenerationRepository _TagGeneration;
        private IBNBHoldReasonsRepository _HoldReasons;
        private IBNBLocationMasterRepository _LocationMaster;

        private IBNBMasterRepository _Master;
        private IBNBProductionStatusRepository _ProductionStatus;

        private IBNBUsersRepository _Users;
        private IBNBProductionEntryRepository _ProductionEntry;
        private IPlantMasterRepository _PlantMaster;
        private IBNBLogStatusRepository _LogStatus;
        private IBNBSupplierRepository _Supplier;
        private IBNBMaterialMappingRepository _MaterialMapping;
        private IBNBReportRepository _Reports;


        public IBNBReportRepository Reports
        {
            get
            {
                if (_Reports == null)
                {
                    _Reports = new BNBReportsRepository(_repoContext);
                }
                return _Reports;
            }
        }
        public IBNBMaterialMappingRepository BNBMaterialMapping
        {
            get
            {
                if (_MaterialMapping == null)
                {
                    _MaterialMapping = new BNBMaterialMappingRepository(_repoContext);
                }
                return _MaterialMapping;
            }
        }

        public IBNBSupplierRepository BNBSupplier
        {
            get
            {
                if (_Supplier == null)
                {
                    _Supplier = new BNBSupplierRepository(_repoContext);
                }
                return _Supplier;
            }
        }

        public IBNBMasterRepository BNBMaster
        {
            get
            {
                if (_Master == null)
                {
                    _Master = new BNBMasterRepository(_repoContext);
                }
                return _Master;
            }
        }

        public IBNBProductionStatusRepository BNBProductionStatus
        {
            get
            {
                if (_ProductionStatus == null)
                {
                    _ProductionStatus = new BNBProductionStatusRepository(_repoContext);
                }
                return _ProductionStatus;
            }
        }

        public IBNBLogStatusRepository BNBLogStatus
        {
            get
            {
                if (_LogStatus == null)
                {
                    _LogStatus = new BNBLogStatusRepository(_repoContext);
                }
                return _LogStatus;
            }
        }

        public IBNBProductionEntryRepository BNBProductionEntry
        {
            get
            {
                if (_ProductionEntry == null)
                {
                    _ProductionEntry = new BNBProductionEntryRepository(_repoContext);
                }
                return _ProductionEntry;
            }
        }
        public IBNBCompoundAgingRelaxationRepository BNBCompoundAgingRelaxation
        {
            get
            {
                if (_CompoundAgingRelaxation == null)
                {
                    _CompoundAgingRelaxation = new BNBCompoundAgingRelaxationRepository(_repoContext);
                }
                return _CompoundAgingRelaxation;
            }
        }

        public IPlantMasterRepository PlantMaster
        {
            get
            {
                if (_PlantMaster == null)
                {
                    _PlantMaster = new PlantMasterRepository(_repoContext);
                }
                return _PlantMaster;
            }
        }

        public IBNBUsersRepository BNBUsers
        {
            get
            {
                if (_Users == null)
                {
                    _Users = new BNBUsersRepository(_repoContext);
                }
                return _Users;
            }
        }
        public IBNBTagGenerationRepository BNBTagGeneration
        {
            get
            {
                if (_TagGeneration == null)
                {
                    _TagGeneration = new BNBTagGenerationRepository (_repoContext);
                }
                return _TagGeneration;
            }
        }

        public IBNBCompoundMasterRepository BNBCompoundMaster
        {
            get
            {
                if (_CompoundMaster == null)
                {
                    _CompoundMaster = new BNBCompoundMasterRepository(_repoContext);
                }
                return _CompoundMaster;
            }
        }


        public IBNBCompoundTypeMasterRepository BNBCompoundTypeMaster
        {
            get
            {
                if (_CompoundTypeMaster == null)
                {
                    _CompoundTypeMaster = new BNBCompoundTypeMasterRepository(_repoContext);
                }
                return _CompoundTypeMaster;
            }
        }

        public IBNBHoldReasonsRepository BNBHoldReasons
        {
            get
            {
                if (_HoldReasons == null)
                {
                    _HoldReasons = new BNBHoldReasonsRepository(_repoContext);
                }
                return _HoldReasons;
            }
        }
        public IBNBHoldRepository BNBHold
        {
            get
            {
                if (_Hold == null)
                {
                    _Hold = new BNBHoldRepository(_repoContext);
                }
                return _Hold;
            }
        }
        public IBNBLocationMasterRepository BNBLocationMaster
        {
            get
            {
                if (_LocationMaster == null)
                {
                    _LocationMaster = new BNBLocationMasterRepository(_repoContext);
                }
                return _LocationMaster;
            }
        }


        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        public void Save()
        {


            _repoContext.SaveChanges();
        }
    }
}
