﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Entities.DataTransferObjects;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace Repository
{
    public class BNBMaterialMappingRepository : RepositoryBase<BNBMaterialMapping>, IBNBMaterialMappingRepository
    {
        private RepositoryContext _context;
        public BNBMaterialMappingRepository(RepositoryContext repositoryContext): base(repositoryContext)
        {
            _context = repositoryContext;
        }


        public string GetMaterialMapping(string CompoundCode)
        {

            var result =  FindByCondition(x => x.Scheduled_Material == CompoundCode);

                var entity = new BNBMaterialMapping();
            string InputMaterial = "";
                if (result.Any())
                {
                entity = result.FirstOrDefault();
                InputMaterial = Convert.ToString(entity.Input_Material);
                }

            return InputMaterial;

        }

        public async Task<IEnumerable<BNBMaterialMapping>> GetAllMaterialMapping()


        {

            return await FindAll().ToListAsync();

        }

        public void CreateMaterialMapping(BNBMaterialMapping MaterialMapping)

        {

            Create(MaterialMapping);
        }
        public void DeleteMaterialMapping(BNBMaterialMapping MaterialMapping)
        {
            Delete(MaterialMapping);
        }

        public BNBMaterialMapping GetMaterialMappingById(Int64 MaterialMapping_Id)
        {
            return FindByCondition(BNB_MaterialMapping => BNB_MaterialMapping.Mapping_ID == MaterialMapping_Id).FirstOrDefault();
        }



        public void UpdateMaterialMapping(BNBMaterialMapping MaterialMapping)
        {


            Update(MaterialMapping);
        }


    }
}
