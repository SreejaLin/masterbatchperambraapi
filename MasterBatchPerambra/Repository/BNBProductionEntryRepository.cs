﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Entities.DataTransferObjects;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace Repository
{
    public class BNBProductionEntryRepository : RepositoryBase<BNBProductionEntry>, IBNBProductionEntryRepository
    {
        private RepositoryContext _context;
        public BNBProductionEntryRepository(RepositoryContext repositoryContext): base(repositoryContext)
        {
            _context = repositoryContext;
        }

        public async Task<IEnumerable<BNBProductionEntry>> GetAllProductionEntry()
        {

            return await FindAll().ToListAsync();

        }

        public void CreateProductionEntry(BNBProductionEntry ProductionEntry)

        {


            Create(ProductionEntry);
        }
        public void DeleteProductionEntry(BNBProductionEntry ProductionEntry)
        {
            Delete(ProductionEntry);
        }

        public BNBProductionEntry GetProductionEntryById(Int64 ProductionEntry_Id)
        {
            return FindByCondition(BNB_Production_Entry => BNB_Production_Entry.BNB_Prod_Id == ProductionEntry_Id).FirstOrDefault();
        }
        public bool CheckAlreadyExisting(string CompCode, int skidno, DateTime shiftdate, string shift,string specialtrail)
        {
            var row = FindByCondition(BNB_Production_Entry => BNB_Production_Entry.Compound_Code == CompCode && BNB_Production_Entry.Skid_NO == skidno && BNB_Production_Entry.Shift_Date == shiftdate && BNB_Production_Entry.Shift == shift && BNB_Production_Entry.Specail_Trails == specialtrail);
            if(row.Any())
            {
                return true;
            }
            return false;

        }

        public async Task<IEnumerable<BNBProductionEntry>> GetFiveLocationforcompounds(string compound_Code)

        {

            return await FindByCondition(BNB_Production_Entry => BNB_Production_Entry.Compound_Code == compound_Code && (DateTime.Now >= BNB_Production_Entry.Ageing_Time && DateTime.Now <= BNB_Production_Entry.Use_Before) && BNB_Production_Entry.Consumtion_Status != "OK" && ((BNB_Production_Entry.Prod_Status == "OK" && BNB_Production_Entry.Lab_Status == "OK") || (BNB_Production_Entry.Tech_Status == "RELEASE" || BNB_Production_Entry.Tech_Status == "DISPOSE"))).Take(5).ToListAsync();
        }
        public async Task<IEnumerable<BNBProductionEntry>> TagRePrintfortrucker(string Shift_Date, string Shift)
        {
            string dshift = Shift;
            DateTime proddate = Convert.ToDateTime(Shift_Date);
            //if (dshift == "C")
            //{
            //    dshift = "C";
            //    proddate = proddate.AddDays(-1);

            //}
            return await  FindByCondition(BNB_Production_Entry => BNB_Production_Entry.Shift_Date == proddate && BNB_Production_Entry.Shift == dshift && BNB_Production_Entry.Lab_Status == null).ToListAsync();

        }

        public async Task<IEnumerable<BNBProductionEntry>> GetProductionEntryForLabApproval()
        {

            return await FindByCondition(BNB_Production_Entry => BNB_Production_Entry.Lab_Status == null || BNB_Production_Entry.Lab_Status == ""  ).ToListAsync();
        }

        public async Task<IEnumerable<BNBProductionEntry>> GetProductionEntryForTechApproval()
        {

            return await FindByCondition(BNB_Production_Entry => BNB_Production_Entry.Tech_Status == null && (  BNB_Production_Entry.Lab_Status == "NOTOK" || BNB_Production_Entry.Prod_Status == "HOLD" && BNB_Production_Entry.Consumtion_Status != "OK" && BNB_Production_Entry.Lab_Remarks != null) ).ToListAsync();
        }


        public async Task<IEnumerable<BNBProductionEntry>> GetUpdatedProdEntryForDateCompondShiftBNB(string bnb, string shift, DateTime dt)//, string compound)

        {

            return await FindByCondition(BNB_Production_Entry => BNB_Production_Entry.Banbury == bnb && BNB_Production_Entry.Shift == shift && BNB_Production_Entry.Shift_Date == dt
            ///&& BNB_Production_Entry.Compound_Code == compound
            && BNB_Production_Entry.Consumtion_Status != "OK" && BNB_Production_Entry.Lab_Status != null).ToListAsync();
        }

        public BNBProductionEntry GetProductionEntryForCurrentTrucker(string truckerCode)

        {

            string dshift = BindCurrentDateandShift();
            DateTime proddate = DateTime.Today;
            if (dshift == "C")
            {
               dshift = "C"; proddate = proddate.AddDays(-1);

            }
            string proddate1 = proddate.ToString("yyyy-MM-dd");
            return FindByCondition(BNB_Production_Entry => BNB_Production_Entry.Trucker_Code == truckerCode && BNB_Production_Entry.Shift ==dshift && BNB_Production_Entry.Shift_Date == proddate && BNB_Production_Entry.Consumtion_Status == null && BNB_Production_Entry.Lab_Status == null).FirstOrDefault();
        }


        //get use before datetime
        public DateTime GetUseBeforeDate()
        {
            string shift = BindCurrentDateandShift();
            string ShiftBendtime, ShiftCendtime,  ShiftAendtime;
            ShiftAendtime = " 13:59:01";
            ShiftBendtime = " 21:59:01";
            ShiftCendtime = " 05:59:01";

            DateTime UseBeforeTime = DateTime.Today.AddDays(11);


            if (shift == "A")
            {
                UseBeforeTime =   Convert.ToDateTime( UseBeforeTime.ToShortDateString() + ShiftCendtime);
            }
           else if (shift == "B")
            {
                UseBeforeTime = Convert.ToDateTime(UseBeforeTime.ToShortDateString() + ShiftCendtime);
            }
          else if (shift == "C")
            {
                UseBeforeTime = Convert.ToDateTime(UseBeforeTime.ToShortDateString() + ShiftCendtime);
            }

            return Convert.ToDateTime(UseBeforeTime);
        }


        public DateTime GetUseBeforeDateprdate(DateTime bnbPrddate)
        {
            string shift = BindCurrentDateandShift();
            string ShiftBendtime, ShiftCendtime, ShiftAendtime;
            ShiftAendtime = " 13:59:01";
            ShiftBendtime = " 21:59:01";
            ShiftCendtime = " 05:59:01";

            DateTime UseBeforeTime = bnbPrddate.AddDays(11);


            if (shift == "A")
            {
                UseBeforeTime = Convert.ToDateTime(UseBeforeTime.ToShortDateString() + ShiftCendtime);
            }
            else if (shift == "B")
            {
                UseBeforeTime = Convert.ToDateTime(UseBeforeTime.ToShortDateString() + ShiftCendtime);
            }
            else if (shift == "C")
            {
                UseBeforeTime = Convert.ToDateTime(UseBeforeTime.ToShortDateString() + ShiftCendtime);
            }

            return Convert.ToDateTime(UseBeforeTime);
        }


        //get last production id and generate barcode
        public string GetBarcodeForProduction(string CompCode)
        {
            String barcode = "BNB";
            string newID = "";
            var entity = new BNBProductionEntry();
            var rows = FindAll().OrderByDescending(x => x.BNB_Prod_Id);
            if (rows.Any()) {
                entity = rows.FirstOrDefault();
                newID = Convert.ToString(entity.BNB_Prod_Id + 1);
                 }
            else
            {
                newID = "1";

            }
           if(CompCode.Contains("M"))
            {
                return barcode + newID + "M";
            }
            else {return barcode + newID + "F"; }

        }

        public void UpdateProductionEntryLabStatus(BNB_LabStatusDTO LabStatus)
        {
             var entity = new BNBProductionEntry();
            var rows = FindByCondition(BNB_Production_Entry => BNB_Production_Entry.BNB_Prod_Id == LabStatus.BNB_Prod_Id );
            if (rows.Any())
            {
                entity = rows.FirstOrDefault();
                entity.Lab_Status = LabStatus.Lab_Status;
                entity.Lab_Remarks = LabStatus.Lab_Remarks;
                entity.Lab_EmpCode = LabStatus.Lab_EmpCode;


            }



            Update(entity);
        }
        public void UpdateConsumeptionStatus(BNB_ConsumptionDTO bNB_Consump)
        {
            var entity = new BNBProductionEntry();
            var rows = FindByCondition(BNB_Production_Entry => BNB_Production_Entry.BNB_Prod_Id == bNB_Consump.bnB_Prod_Id && ( BNB_Production_Entry.Consumtion_Status == null || BNB_Production_Entry.Consumtion_Status == ""));
            if (rows.Any())
            {
                entity = rows.FirstOrDefault();
                entity.Consumtion_Status = "OK";
                entity.Consumption_Time = DateTime.Now;
                entity.ForkliftOpt_Code = bNB_Consump.ForkliftOpt_Code;
                entity.Consumption_BNB = bNB_Consump.consumptionBnb;


            }



            Update(entity);
        }

        public void EditProductionEntryLabStatus(BNB_LabStatusDTO LabStatus)
        {
            var entity = new BNBProductionEntry();
            var rows = FindByCondition(BNB_Production_Entry => BNB_Production_Entry.BNB_Prod_Id == LabStatus.BNB_Prod_Id);
            if (rows.Any())
            {
                entity = rows.FirstOrDefault();
                entity.Lab_Status = LabStatus.Lab_Status;
                entity.Lab_Remarks = LabStatus.Lab_Remarks;
                entity.Lab_EmpCode = LabStatus.Lab_EmpCode;


            }



            Update(entity);
        }


        public void UpdateProductionEntryTechStatus(BNB_TechStatusDTO TechStatus)
        {
            var entity = new BNBProductionEntry();
            var rows = FindByCondition(BNB_Production_Entry => BNB_Production_Entry.BNB_Prod_Id == TechStatus.BNB_Prod_Id );
            if (rows.Any())
            {
                entity = rows.FirstOrDefault();
                entity.Tech_Status = TechStatus.Tech_Status;
                entity.Tech_Remarks = TechStatus.Tech_Remarks;
                entity.Tech_EmpCode = TechStatus.Tech_EmpCode;
                if(TechStatus.Use_Before != entity.Use_Before)
                {
                    entity.Use_Before = TechStatus.Use_Before;

                }

            }



            Update(entity);
        }

        //Fetch compounds for ageing relaxation
        public async Task<IEnumerable<BNBProductionEntry>> GetCompoundsForRelaxation(string compound)//, string time)
        {
           // double RelaxedHours = Convert.ToDouble(time);
            return await FindByCondition( bnbprod => bnbprod.Compound_Code == compound && DateTime.Now <= bnbprod.Ageing_Time  && bnbprod.Lab_Status == "OK" && bnbprod.Prod_Status == "OK").ToListAsync();
        }

        public void RelaxAgeforCompoundProdId(BNB_TechStatusDTO TechStatus)
        {
            var entity = new BNBProductionEntry();
            var rows = FindByCondition(BNB_Production_Entry => BNB_Production_Entry.BNB_Prod_Id == TechStatus.BNB_Prod_Id);
            if (rows.Any())
            {
                entity = rows.FirstOrDefault();
                double RelaxedHours = Convert.ToDouble(TechStatus.RelaxedTime);
                entity.Ageing_Time = entity.Ageing_Time.AddHours(-RelaxedHours);

               // entity.Tech_Status = TechStatus.Tech_Status;
                entity.Tech_Remarks = TechStatus.Tech_Remarks;
                entity.Tech_EmpCode = TechStatus.Tech_EmpCode;


            }



            Update(entity);
        }




        // Fetch PPC schdeule using linked server
        public List<BNB_PPCScheduleDTO> GetPPCScheduleForBNB(string BNB, string shift)
        {
            string cshift = BindCurrentDateandShift();
            string dshift = cshift;
            DateTime proddate = DateTime.Today;
            string BanburyView = "BNB_DailyProduction" + BNB;
            if (shift == "PreviousShift")
            {
                if (cshift == "A") { dshift = "C"; proddate = proddate.AddDays(-1); }
                else if (cshift == "B") { dshift = "A"; }
                else if (cshift == "C") { dshift = "B"; }

            }
           string proddate1 = proddate.ToString("yyyy-MM-dd");
            var cnn = _context.Database.GetConnectionString();
            //Using View
            //string sp = "SELECT  [Prod_Date] ,[Shift] ,[RPM],[Priority_No],[Comp_Code],[Sch_Remarks],[Sch_Value],[C_Status] FROM [BanburyFIFO].[dbo].[" + BanburyView+ "] where Shift = '"+ dshift + "' and Prod_Date = '" + proddate1 + "'";

            //Using Linked server
            string sp = "SELECT  [Prod_Date] ,[Shift] ,[RPM],[Priority_No],[Comp_Code],[Sch_Remarks],[Sch_Value],[C_Status] FROM [PRMBSQLS01\\DATASERVER].[Apollo_Cochin].[dbo].[" + BanburyView + "] where Shift = '" + dshift + "' and Prod_Date = '" + proddate1 + "'";

            //int resultsIndex = 0;
            var resultslist = new List<BNB_PPCScheduleDTO>();

            var databaseResult = SqlHelper.ExecuteDataTable(cnn, CommandType.Text, sp);
            if (databaseResult.Rows != null)
            {

                foreach (DataRow dataRow in databaseResult.Rows)

                {
                    var resultsRow = new BNB_PPCScheduleDTO();
                    if (dataRow.GetValue("Comp_Code") != DBNull.Value)
                    {
                        resultsRow.Comp_Code = Convert.ToString(dataRow.GetValue("Comp_Code"));
                    }
                    if (dataRow.GetValue("Sch_Remarks") != DBNull.Value)
                    {
                        resultsRow.Sch_Remarks = Convert.ToString(dataRow.GetValue("Sch_Remarks"));
                    }
                    if (dataRow.GetValue("Sch_value") != DBNull.Value)
                    {
                        resultsRow.Sch_value = Convert.ToInt32(dataRow.GetValue("Sch_value"));
                    }
                    if (dataRow.GetValue("RPM") != DBNull.Value)
                    {
                        resultsRow.RPM = Convert.ToString(dataRow.GetValue("RPM"));

                    }

                    resultsRow.Date = DateTime.Today.ToString("dd-MM-yyy", CultureInfo.InvariantCulture);
                    resultsRow.shift = cshift;
                    resultslist.Add(resultsRow);

                    //resultsIndex++;
                }

            }
            return resultslist;

        }

        public List<BNB_PPCScheduleDTO> GetPPCScheduleForDates(string fdate, string shift,string bnb)
        {

            DateTime proddate = Convert.ToDateTime(fdate);
            string BanburyView = "BNB_DailyProduction" + bnb;

           string proddate1 = proddate.ToString("yyyy-MM-dd");
            var cnn = _context.Database.GetConnectionString();
            //Using View
            //string sp = "SELECT  [Prod_Date] ,[Shift] ,[RPM],[Priority_No],  [Comp_Code],[Sch_Remarks],[Sch_Value],[C_Status] FROM [BanburyFIFO].[dbo].[" + BanburyView+ "] where Shift = '"+ dshift + "' and Prod_Date = '" + proddate1 + "'";

            //Using Linked server
            string sp = "SELECT  [Prod_Date] ,[Shift] ,[RPM],[Priority_No],[Comp_Code],[Sch_Remarks],[Sch_Value],[C_Status] FROM [PRMBSQLS01\\DATASERVER].[Apollo_Cochin].[dbo].[" + BanburyView + "] where Shift = '" + shift + "' and Prod_Date = '" + proddate1 + "'";

            //int resultsIndex = 0;
            var resultslist = new List<BNB_PPCScheduleDTO>();

            var databaseResult = SqlHelper.ExecuteDataTable(cnn, CommandType.Text, sp);
            if (databaseResult.Rows != null)
            {

                foreach (DataRow dataRow in databaseResult.Rows)

                {
                    var resultsRow = new BNB_PPCScheduleDTO();
                    if (dataRow.GetValue("Comp_Code") != DBNull.Value)
                    {
                        resultsRow.Comp_Code = Convert.ToString(dataRow.GetValue("Comp_Code"));
                    }
                    if (dataRow.GetValue("Sch_Remarks") != DBNull.Value)
                    {
                        resultsRow.Sch_Remarks = Convert.ToString(dataRow.GetValue("Sch_Remarks"));
                    }
                    if (dataRow.GetValue("Sch_value") != DBNull.Value)
                    {
                        resultsRow.Sch_value = Convert.ToInt32(dataRow.GetValue("Sch_value"));
                    }
                    if (dataRow.GetValue("RPM") != DBNull.Value)
                    {
                        resultsRow.RPM = Convert.ToString(dataRow.GetValue("RPM"));

                    }

                    resultsRow.Date = Convert.ToDateTime(proddate1).ToString("dd-MM-yyy", CultureInfo.InvariantCulture);



                    resultsRow.shift = shift;
                    resultslist.Add(resultsRow);

                    //resultsIndex++;
                }

            }
            return resultslist;

        }




        //Getting Current Shift/Previous Shift
        public string BindCurrentDateandShift()
        {
            string shift;
            DateTime ShiftB, ShiftC, EndDayTime, NextDayStartTime, lastDay, ShiftA;
            DateTime now = DateTime.Now;
            DateTime curTime = DateTime.Now;
            ShiftA = Convert.ToDateTime("06:01:00 AM");
            ShiftB = Convert.ToDateTime("02:01:00 PM");
            ShiftC = Convert.ToDateTime("10:01:00 PM");
            EndDayTime = Convert.ToDateTime("11:59:55 PM");
            NextDayStartTime = Convert.ToDateTime("12:00:00 AM");

            if (curTime >= ShiftA && curTime < ShiftB)
            { shift = "A"; }
            else if (curTime >= ShiftB && curTime < ShiftC)
            { shift = "B"; }
            else if (curTime >= ShiftC && curTime <= EndDayTime)
            { shift = "C"; }
            else if (curTime >= NextDayStartTime && curTime < ShiftA)
            { shift = "C"; }
            else
            { shift = "G"; }
            return shift;
        }




    }
}
