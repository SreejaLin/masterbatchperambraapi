﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBLocationMasterRepository : RepositoryBase<BNBLocationMaster>, IBNBLocationMasterRepository
    {
        public BNBLocationMasterRepository(RepositoryContext repositoryContext)
      : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBLocationMaster>> GetAlllocations()


        {

            return await FindAll()
              //  .Include(Y => Y.PlantMaster)
                .ToListAsync();

        }
        public bool GetlocationStatusByName(string location)
        {

            var BNBLocationMaster = new BNBLocationMaster();
            if (location != null)
            {

                var data = FindByCondition(BNB_Location_Master => BNB_Location_Master.BNB_Location_Name == location);
                if (data.Any()) { BNBLocationMaster = data.FirstOrDefault(); return BNBLocationMaster.BNB_Location_Status;
                
             }

                return BNBLocationMaster.BNB_Location_Status;

            }
            return false;
        }
        public void Createlocation(BNBLocationMaster location)

        {

            Create(location);
        }
        public void Deletelocation(BNBLocationMaster location)
        {
            Delete(location);
        }

        public BNBLocationMaster GetlocationById(Int64 location_Id)
        {
            return FindByCondition(BNB_Location_Master => BNB_Location_Master.BNB_Location_id == location_Id).FirstOrDefault();
        }

        public BNBLocationMaster GetlocationByNamebanbury(Int64 bnb, string location_Name)
        {
            return FindByCondition(BNB_Location_Master => BNB_Location_Master.BNB_Location_Name == location_Name).FirstOrDefault();
        }
        public BNBLocationMaster GetEmptyLocation()
        {
            return FindByCondition(BNB_Location_Master => BNB_Location_Master.BNB_Location_Status == true).FirstOrDefault();
        }
        public void Updatelocation(BNBLocationMaster location)
        {


            Update(location);
        }
        public void UpdatelocationByName(string location)
        {
            var BNBLocationMaster = new BNBLocationMaster();
            if( location != null)
            {

                var data = FindByCondition(BNB_Location_Master => BNB_Location_Master.BNB_Location_Name == location);
                if (data.Any()) { BNBLocationMaster = data.FirstOrDefault(); }
                if (BNBLocationMaster.BNB_Location_Status = true)
                {
                    BNBLocationMaster.BNB_Location_Status = false;
                }
                else
                {
                    BNBLocationMaster.BNB_Location_Status = true;
                }
            }
            Update(BNBLocationMaster);
        }
    }
}
