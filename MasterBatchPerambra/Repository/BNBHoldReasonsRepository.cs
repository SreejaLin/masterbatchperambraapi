﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBHoldReasonsRepository : RepositoryBase<BNBHoldReasons>, IBNBHoldReasonsRepository
    {
        public BNBHoldReasonsRepository(RepositoryContext repositoryContext)
    : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBHoldReasons>> GetAllholdreasons()


        {

            return await FindAll().ToListAsync();

        }

        public void CreateHoldReasons(BNBHoldReasons HoldReasons)

        {

            Create(HoldReasons);
        }
        public void DeleteHoldReasons(BNBHoldReasons HoldReasons)
        {
            Delete(HoldReasons);
        }

        public BNBHoldReasons GetHoldReasonsById(Int64 HoldReasons_Id)
        {
            return FindByCondition(BNB_Hold_Reasons => BNB_Hold_Reasons.Hold_reason_id == HoldReasons_Id).FirstOrDefault();
        }

        public BNBHoldReasons GetHoldReasonsByNamebanbury(Int64 bnb, string HoldReasons_Name)
        {
            return FindByCondition(BNB_Hold_Reasons => BNB_Hold_Reasons.Hold_reason == HoldReasons_Name).FirstOrDefault();
        }


        public void UpdateHoldReasons(BNBHoldReasons HoldReasons)
        {


            Update(HoldReasons);
        }
    }
}
