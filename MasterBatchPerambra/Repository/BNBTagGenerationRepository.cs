﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBTagGenerationRepository : RepositoryBase<BNBTagGeneration>, IBNBTagGenerationRepository
    {
        public BNBTagGenerationRepository(RepositoryContext repositoryContext)
    : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBTagGeneration>> GetAllTagGeneration()


        {

            return await FindAll().ToListAsync();

        }

        public void CreateTagGeneration(BNBTagGeneration TagGeneration)

        {

            Create(TagGeneration);
        }
        public void DeleteTagGeneration(BNBTagGeneration TagGeneration)
        {
            Delete(TagGeneration);
        }

        public BNBTagGeneration GetTagGenerationById(int TagGeneration_Id)
        {
            return FindByCondition(BNB_TagGeneration => BNB_TagGeneration.TagId == TagGeneration_Id).FirstOrDefault();
        }

       


        public void UpdateTagGeneration(BNBTagGeneration TagGeneration)
        {


            Update(TagGeneration);
        }
    }
}
