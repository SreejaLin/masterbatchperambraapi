﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Entities.DataTransferObjects;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace Repository
{
    public class BNBReportsRepository : RepositoryBase<BNBProductionEntry>, IBNBReportRepository
    {
        private RepositoryContext _context;
        public BNBReportsRepository(RepositoryContext repositoryContext): base(repositoryContext)
        {
            _context = repositoryContext;
        }


        public async Task<IEnumerable<BNBProductionEntry>> GetAllProductionReport(DateTime FromDate, DateTime ToDate)
        {

            return await FindByCondition(x =>( x.Shift_Date >= FromDate && x.Shift_Date <= ToDate) && x.Specail_Trails != "O").ToListAsync();

        }
        /// <summary>
        /// logic for method goes here
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public async Task<IEnumerable<BNBProductionEntry>> GetAllConsumptionReport(DateTime FromDate, DateTime ToDate)
        {

            return await FindByCondition(x => (x.Shift_Date >= FromDate && x.Shift_Date <= ToDate) && x.Specail_Trails != "O").ToListAsync();

        }

        public async Task<IEnumerable<BNBProductionEntry>> GetAllCurrentstockReport()
        {

            return await FindByCondition(x => x.Prod_Status == "OK" && x.Lab_Status =="OK" && x.Consumtion_Status != "OK").ToListAsync();

        }
        public async Task<IEnumerable<BNBProductionEntry>> GetAllOveragedReport()
        {

            return await FindByCondition(x => DateTime.Now > x.Use_Before && x.Consumtion_Status != "OK").ToListAsync();

        }

        // Fetch PPC schdeule using linked server
        //public List<BNB_Production_EntryDTO> GetProductionReport(string BNB, string shift)
        //{
        //    string dshift = BindCurrentDateandShift();
        //    DateTime proddate = DateTime.Today;
        //    string BanburyView = "BNB_DailyProduction" + BNB;
        //    if (shift == "PreviousShift")
        //    {
        //        if (dshift == "A") { dshift = "C"; proddate = proddate.AddDays(-1); }
        //        else if (dshift == "B") { dshift = "A"; }
        //        else if (dshift == "C") { dshift = "B"; }

        //    }
        //   string proddate1 = proddate.ToString("yyyy-MM-dd");
        //    var cnn = _context.Database.GetConnectionString();
        //    //Using View
        //    //string sp = "SELECT  [Prod_Date] ,[Shift] ,[RPM],[Priority_No],[Comp_Code],[Sch_Remarks],[Sch_Value],[C_Status] FROM [BanburyFIFO].[dbo].[" + BanburyView+ "] where Shift = '"+ dshift + "' and Prod_Date = '" + proddate1 + "'";

        //    //Using Linked server
        //    string sp = "SELECT  * FROM [PRMBSQLS01\\DATASERVER].[Apollo_Cochin].[dbo].[" + BanburyView + "] where Shift = '" + dshift + "' and Prod_Date = '" + proddate1 + "'";

        //    //int resultsIndex = 0;
        //    var resultslist = new List<BNB_PPCScheduleDTO>();

        //    var databaseResult = SqlHelper.ExecuteDataTable(cnn, CommandType.Text, sp);
        //    if (databaseResult.Rows != null)
        //    {

        //        foreach (DataRow dataRow in databaseResult.Rows)

        //        {
        //            var resultsRow = new BNB_PPCScheduleDTO();
        //            if (dataRow.GetValue("Comp_Code") != DBNull.Value)
        //            {
        //                resultsRow.Comp_Code = Convert.ToString(dataRow.GetValue("Comp_Code"));
        //            }
        //            if (dataRow.GetValue("Sch_Remarks") != DBNull.Value)
        //            {
        //                resultsRow.Sch_Remarks = Convert.ToString(dataRow.GetValue("Sch_Remarks"));
        //            }
        //            if (dataRow.GetValue("Sch_value") != DBNull.Value)
        //            {
        //                resultsRow.Sch_value = Convert.ToInt32(dataRow.GetValue("Sch_value"));
        //            }
        //            if (dataRow.GetValue("RPM") != DBNull.Value)
        //            {
        //                resultsRow.RPM = Convert.ToString(dataRow.GetValue("RPM"));

        //            }

        //            resultsRow.Date = Convert.ToDateTime(proddate1).ToString("dd-MM-yyy", CultureInfo.InvariantCulture); 



        //            resultsRow.shift = dshift;
        //            resultslist.Add(resultsRow);

        //            //resultsIndex++;
        //        }

        //    }
        //    return resultslist;

        //}





        //Getting Current Shift/Previous Shift
        public string BindCurrentDateandShift()
        {
            string shift;
            DateTime ShiftB, ShiftC, EndDayTime, NextDayStartTime, lastDay, ShiftA;
            DateTime now = DateTime.Now;
            DateTime curTime = DateTime.Now;
            ShiftA = Convert.ToDateTime("06:01:00 AM");
            ShiftB = Convert.ToDateTime("02:01:00 PM");
            ShiftC = Convert.ToDateTime("10:01:00 PM");
            EndDayTime = Convert.ToDateTime("11:59:55 PM");
            NextDayStartTime = Convert.ToDateTime("12:00:00 AM");

            if (curTime >= ShiftA && curTime < ShiftB)
            { shift = "A"; }
            else if (curTime >= ShiftB && curTime < ShiftC)
            { shift = "B"; }
            else if (curTime >= ShiftC && curTime <= EndDayTime)
            { shift = "C"; }
            else if (curTime >= NextDayStartTime && curTime < ShiftA)
            { shift = "C"; }
            else
            { shift = "G"; }
            return shift;
        }




    }
}
