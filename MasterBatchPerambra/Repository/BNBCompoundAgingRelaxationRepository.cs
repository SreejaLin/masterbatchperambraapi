﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Entities.Models;
using Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class BNBCompoundAgingRelaxationRepository : RepositoryBase<BNBCompoundAgingRelaxation>, IBNBCompoundAgingRelaxationRepository
    {
        public BNBCompoundAgingRelaxationRepository(RepositoryContext repositoryContext)
: base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BNBCompoundAgingRelaxation>> GetAllCompoundAgingRelaxation()


        {

            return await FindAll().ToListAsync();

        }

        public void CreateCompoundAgingRelaxation(BNBCompoundAgingRelaxation CompoundAgingRelaxation)

        {

            Create(CompoundAgingRelaxation);
        }
        public void DeleteCompoundAgingRelaxation(BNBCompoundAgingRelaxation CompoundAgingRelaxation)
        {
            Delete(CompoundAgingRelaxation);
        }

        public BNBCompoundAgingRelaxation GetCompoundAgingRelaxationById(Int64 CompoundAgingRelaxation)
        {
            return FindByCondition(BNB_Compound_Aging_Relaxation => BNB_Compound_Aging_Relaxation.Aging_relaxation_id == CompoundAgingRelaxation).FirstOrDefault();
        }

        //public BNB_Compound_Aging_Relaxation GetCompoundAgingRelaxationByNamebanbury(Int64 bnb, string Compound_Name)
        //{
        //    return FindByCondition(BNB_Compound_Aging_Relaxation => BNB_Compound_Aging_Relaxation.Aging_relaxation_id == Compound_Name).FirstOrDefault();
        //}


        public void UpdateCompound(BNBCompoundAgingRelaxation CompoundAgingRelaxation)
        {


            Update(CompoundAgingRelaxation);
        }
    }
}
