﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBMaterialMappingRepository : IRepositoryBase<BNBMaterialMapping>
    {
        string GetMaterialMapping(string CompoundCode);


        Task<IEnumerable<BNBMaterialMapping>> GetAllMaterialMapping();



        void CreateMaterialMapping(BNBMaterialMapping MaterialMapping);


        void DeleteMaterialMapping(BNBMaterialMapping MaterialMapping);


        BNBMaterialMapping GetMaterialMappingById(Int64 MaterialMapping_Id);




        void UpdateMaterialMapping(BNBMaterialMapping MaterialMapping);


    }
}
