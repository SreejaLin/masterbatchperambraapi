﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBLogStatusRepository : IRepositoryBase<BNBLogStatus>
    {
        Task<IEnumerable<BNBLogStatus>> GetAllLogStatus();



        void CreateLogStatus(BNBLogStatus LogStatus);


        void DeleteLogStatus(BNBLogStatus LogStatus);

        BNBLogStatus GetLogStatusById(Int32 LogStatus_Id);


        Task<IEnumerable<BNBLogStatus>> GetUpdatedProdEntryForLabEmp(string labemp);//, string compound)


        void UpdateLogStatus(BNBLogStatus LogStatus);
       

    }
}
