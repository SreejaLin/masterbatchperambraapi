﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBSupplierRepository : IRepositoryBase<BNBSupplier>
    {

        Task<IEnumerable<BNBSupplier>> GetAllSupplier();




        void CreateSupplier(BNBSupplier Supplier);


        void DeleteSupplier(BNBSupplier Supplier);

        BNBSupplier GetSupplierById(Int64 Supplier_Id);



        void UpdateSupplier(BNBSupplier Supplier);


    }
}
