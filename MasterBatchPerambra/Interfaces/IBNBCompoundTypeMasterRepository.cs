﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace Interfaces
{
    public interface IBNBCompoundTypeMasterRepository : IRepositoryBase<BNBCompoundTypeMaster>
    {
        Task<IEnumerable<BNBCompoundTypeMaster>> GetAllCompoundType();



        void CreateCompoundType(BNBCompoundTypeMaster CompoundType);

        void DeleteCompoundType(BNBCompoundTypeMaster CompoundType);

        BNBCompoundTypeMaster GetCompoundTypeById(Int64 CompoundType_Id);

        BNBCompoundTypeMaster GetCompoundTypeByNamebanbury(Int64 bnb, string CompoundType_Name);



        void UpdateCompoundType(BNBCompoundTypeMaster CompoundType);
        

    }
}
