﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBProductionStatusRepository : IRepositoryBase<BNBProductionStatus>
    {
        Task<IEnumerable<BNBProductionStatus>> GetAllproduction_Statuss();



        void Createproduction_Status(BNBProductionStatus production_Status);


        void Deleteproduction_Status(BNBProductionStatus production_Status);


        BNBProductionStatus Getproduction_StatusById(Int64 status_Id);


        BNBProductionStatus Getproduction_StatusByName(string production_Status_Name);


        void Updateproduction_Status(BNBProductionStatus production_Status);
       

    }
}
