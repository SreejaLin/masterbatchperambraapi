﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBHoldRepository : IRepositoryBase<BNBHold>
    {
        Task<IEnumerable<BNBHold>> GetAllHold();



        void CreateHold(BNBHold Hold);


        void DeleteHold(BNBHold Hold);

        BNBHold GetHoldById(Int32 Hold_Id);





        void UpdateHold(BNBHold Hold);
       

    }
}
