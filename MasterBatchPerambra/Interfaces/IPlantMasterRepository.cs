﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IPlantMasterRepository : IRepositoryBase<PlantMaster>
    {
        Task<IEnumerable<PlantMaster>> GetAllPlants();



        void CreatePlant(PlantMaster plant);


        void DeletePlant(PlantMaster plant);

        PlantMaster GetPlantById(Int64 plant_Id);

        PlantMaster GetPlantByNameCompany(Int64 Company_Id, string Plant_Name);


        void UpdatePlant(PlantMaster plant);
        
    }
}
