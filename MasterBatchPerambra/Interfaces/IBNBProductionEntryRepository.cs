﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Entities;
using Entities.DataTransferObjects;

namespace Interfaces
{
   public interface IBNBProductionEntryRepository  : IRepositoryBase<BNBProductionEntry>
    {

        Task<IEnumerable<BNBProductionEntry>> GetAllProductionEntry();
        Task<IEnumerable<BNBProductionEntry>>  GetProductionEntryForLabApproval();

        Task<IEnumerable<BNBProductionEntry>> GetProductionEntryForTechApproval();
        void CreateProductionEntry(BNBProductionEntry ProductionEntry);
        Task<IEnumerable<BNBProductionEntry>> GetFiveLocationforcompounds(string compound_Code);
        Task<IEnumerable<BNBProductionEntry>> TagRePrintfortrucker(string Shift_Date, string Shift);
       // Task<IEnumerable<BNBProductionEntry>> GetscheduleForTagPrintforSO(string Shift_Date, string Shift);
        void DeleteProductionEntry(BNBProductionEntry ProductionEntry);

        BNBProductionEntry GetProductionEntryById(Int64 ProductionEntry_Id);

        void UpdateProductionEntryLabStatus(BNB_LabStatusDTO LabStatus);
        void UpdateConsumeptionStatus(BNB_ConsumptionDTO bNB_Consump);
         void EditProductionEntryLabStatus(BNB_LabStatusDTO LabStatus);
        void UpdateProductionEntryTechStatus(BNB_TechStatusDTO TechStatus);
        List<BNB_PPCScheduleDTO> GetPPCScheduleForBNB(string BNB, string shift);
        //Task<IEnumerable<BNBProductionEntry>> GetUpdatedProdEntryForLabEmp( string   labemp);
        List<BNB_PPCScheduleDTO> GetPPCScheduleForDates(string fdate, string shift, string bnb);
        Task<IEnumerable<BNBProductionEntry>> GetUpdatedProdEntryForDateCompondShiftBNB(string bnb, string shift, DateTime dt);//, string compound);
        bool CheckAlreadyExisting(string CompCode, int skidno, DateTime shiftdate, string shift, string specialtrail);
        string GetBarcodeForProduction(string CompCode);
        Task<IEnumerable<BNBProductionEntry>> GetCompoundsForRelaxation(string compound);//, string time);

        void RelaxAgeforCompoundProdId(BNB_TechStatusDTO TechStatus);
        DateTime GetUseBeforeDate();
        DateTime GetUseBeforeDateprdate(DateTime bnbPrddate);
    }
}
