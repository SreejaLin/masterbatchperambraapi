﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public interface IRepositoryWrapper
    {
		IBNBCompoundAgingRelaxationRepository BNBCompoundAgingRelaxation { get; }

		IBNBCompoundMasterRepository BNBCompoundMaster { get; }
		IBNBCompoundTypeMasterRepository BNBCompoundTypeMaster { get; }

		IBNBHoldReasonsRepository BNBHoldReasons { get; }
		IBNBLocationMasterRepository BNBLocationMaster { get; }
		IBNBMasterRepository BNBMaster { get; }
		IBNBTagGenerationRepository BNBTagGeneration { get; }
		IBNBProductionStatusRepository BNBProductionStatus { get; }
		IBNBUsersRepository BNBUsers { get; }
		IPlantMasterRepository PlantMaster { get; }
		IBNBProductionEntryRepository BNBProductionEntry { get; }
		IBNBHoldRepository BNBHold { get; }
		IBNBLogStatusRepository BNBLogStatus { get; }
		IBNBSupplierRepository BNBSupplier { get; }
		IBNBMaterialMappingRepository BNBMaterialMapping { get; }
		IBNBReportRepository Reports { get; }
		void Save();
    }
}
