﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Entities;
using Entities.DataTransferObjects;

namespace Interfaces
{
   public interface IBNBReportRepository : IRepositoryBase<BNBProductionEntry>
    {



        Task<IEnumerable<BNBProductionEntry>> GetAllProductionReport(DateTime FromDate, DateTime ToDate);

        Task<IEnumerable<BNBProductionEntry>> GetAllConsumptionReport(DateTime FromDate, DateTime ToDate);
        Task<IEnumerable<BNBProductionEntry>> GetAllCurrentstockReport();
        Task<IEnumerable<BNBProductionEntry>> GetAllOveragedReport();



    }


}
