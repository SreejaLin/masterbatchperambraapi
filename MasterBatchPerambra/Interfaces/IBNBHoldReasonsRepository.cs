﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBHoldReasonsRepository : IRepositoryBase<BNBHoldReasons>
    {
        Task<IEnumerable<BNBHoldReasons>> GetAllholdreasons();



        void CreateHoldReasons(BNBHoldReasons HoldReasons);


        void DeleteHoldReasons(BNBHoldReasons HoldReasons);

        BNBHoldReasons GetHoldReasonsById(Int64 HoldReasons_Id);

        BNBHoldReasons GetHoldReasonsByNamebanbury(Int64 bnb, string HoldReasons_Name);



        void UpdateHoldReasons(BNBHoldReasons HoldReasons);
       

    }
}
