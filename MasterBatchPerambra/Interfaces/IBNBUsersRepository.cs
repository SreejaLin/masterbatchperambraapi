﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBUsersRepository : IRepositoryBase<BNBUsers>
    {

        Task<IEnumerable<BNBUsers>> GetAllUsers();




        void CreateUser(BNBUsers User);


        void DeleteUser(BNBUsers User);

        BNBUsers GetUserById(Int64 User_Id);


        BNBUsers GetUserByNameCompany(Int64 Company_Id, int User_Name);

        void UpdateUser(BNBUsers User);
        

    }
}
