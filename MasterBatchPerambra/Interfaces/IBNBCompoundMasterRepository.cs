﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBCompoundMasterRepository : IRepositoryBase<BNBCompoundMaster>
    {
        Task<IEnumerable<BNBCompoundMaster>> GetAllCompound();


        void CreateCompound(BNBCompoundMaster Compound);


        void DeleteCompound(BNBCompoundMaster Compound);

        BNBCompoundMaster GetCompoundById(Int64 Compound_Id);

        BNBCompoundMaster GetCompoundByNamebanbury(Int64 bnb, string Compound_Name);



        void UpdateCompound(BNBCompoundMaster Compound);
       
    }
}
