﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBTagGenerationRepository : IRepositoryBase<BNBTagGeneration>
    {
        Task<IEnumerable<BNBTagGeneration>> GetAllTagGeneration();



        void CreateTagGeneration(BNBTagGeneration TagGeneration);


        void DeleteTagGeneration(BNBTagGeneration TagGeneration);

        BNBTagGeneration GetTagGenerationById(Int32 TagGeneration_Id);





        void UpdateTagGeneration(BNBTagGeneration TagGeneration);
       

    }
}
