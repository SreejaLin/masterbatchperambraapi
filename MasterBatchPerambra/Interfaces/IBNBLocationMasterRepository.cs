﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBLocationMasterRepository : IRepositoryBase<BNBLocationMaster>
    {

        Task<IEnumerable<BNBLocationMaster>> GetAlllocations();


        bool GetlocationStatusByName(string location);
        void Createlocation(BNBLocationMaster location);


        void Deletelocation(BNBLocationMaster location);

        BNBLocationMaster GetlocationById(Int64 location_Id);

        BNBLocationMaster GetlocationByNamebanbury(Int64 bnb, string location_Name);



        void Updatelocation(BNBLocationMaster location);
        void UpdatelocationByName(string location);

    }
}
