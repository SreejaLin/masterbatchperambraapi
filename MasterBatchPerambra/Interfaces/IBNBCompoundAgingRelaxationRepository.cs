﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities.Models;
using System.Threading.Tasks;

namespace Interfaces
{
   public interface IBNBCompoundAgingRelaxationRepository : IRepositoryBase<BNBCompoundAgingRelaxation>
    {
        Task<IEnumerable<BNBCompoundAgingRelaxation>> GetAllCompoundAgingRelaxation();



        void CreateCompoundAgingRelaxation(BNBCompoundAgingRelaxation CompoundAgingRelaxation);

        void DeleteCompoundAgingRelaxation(BNBCompoundAgingRelaxation CompoundAgingRelaxation);

        BNBCompoundAgingRelaxation GetCompoundAgingRelaxationById(Int64 CompoundAgingRelaxation);


       // BNB_Compound_Aging_Relaxation GetCompoundAgingRelaxationByNamebanbury(Int64 bnb, string Compound_Name);
       

        void UpdateCompound(BNBCompoundAgingRelaxation CompoundAgingRelaxation);
       
    }
}
