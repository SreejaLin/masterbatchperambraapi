﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
namespace Interfaces
{
    public interface IBNBMasterRepository : IRepositoryBase<BNBMaster>
    {
        Task<IEnumerable<BNBMaster>> GetAllbanburys();


        void Createbanbury(BNBMaster banbury);


        void Deletebanbury(BNBMaster banbury);

        BNBMaster GetbanburyById(Int64 banbury_Id);

        BNBMaster GetbanburyByName(string banbury_Name);



        void Updatebanbury(BNBMaster banbury);
       

    }
}
