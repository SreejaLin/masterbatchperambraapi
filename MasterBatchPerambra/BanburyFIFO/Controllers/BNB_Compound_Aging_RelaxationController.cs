﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace BanburyFIFO.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BNB_Compound_Aging_RelaxationController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;


        public BNB_Compound_Aging_RelaxationController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {

            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]

        public async Task<IEnumerable<BNB_Compound_Aging_RelaxationDTO>> GetAllCompoundAgingRelaxation()
        {
            try
            {
                var BNB_Compound_Aging_Relaxation = await _repository.BNBCompoundAgingRelaxation.GetAllCompoundAgingRelaxation();
                _logger.LogInfo($"Returned all CompoundAgingRelaxation from database.");
                var BNB_Compound_Aging_RelaxationResult = _mapper.Map<IEnumerable<BNB_Compound_Aging_RelaxationDTO>>(BNB_Compound_Aging_Relaxation);
                return BNB_Compound_Aging_RelaxationResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllCompoundAgingRelaxation action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Compound_Aging_RelaxationDTO>)StatusCode(500, "Internal server error");
            }

        }


        [HttpPost]
        public IActionResult CreateCompoundAgingRelaxation([FromBody] BNB_Compound_Aging_RelaxationDTO CompoundAgingRelaxation)
        {
            try
            {
                if (CompoundAgingRelaxation == null)
                {
                    _logger.LogError("CompoundAgingRelaxation object sent from client is null.");
                    return BadRequest("CompoundAgingRelaxation object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid CompoundAgingRelaxation object sent from client.");
                    return BadRequest("Invalid model object");
                }
               

                // CompoundAgingRelaxation.CompoundAgingRelaxation_Id = new Guid();
                var CompoundAgingRelaxationEntity = _mapper.Map<BNBCompoundAgingRelaxation>(CompoundAgingRelaxation);




                _repository.BNBCompoundAgingRelaxation.CreateCompoundAgingRelaxation(CompoundAgingRelaxationEntity);
                _repository.Save();

                var createdCompoundAgingRelaxation = _mapper.Map<BNB_Compound_Aging_RelaxationDTO>(CompoundAgingRelaxationEntity);
                return CreatedAtAction("GetAllCompoundAgingRelaxation", new { id = createdCompoundAgingRelaxation.Aging_relaxation_id }, createdCompoundAgingRelaxation);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateCompoundAgingRelaxation action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteCompoundAgingRelaxation(int id)
        {
            try
            {
                var CompoundAgingRelaxation = _repository.BNBCompoundAgingRelaxation.GetCompoundAgingRelaxationById(id);
                if (CompoundAgingRelaxation == null)
                {
                    _logger.LogError($"CompoundAgingRelaxation with Code: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.BNBCompoundAgingRelaxation.DeleteCompoundAgingRelaxation(CompoundAgingRelaxation);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public IActionResult UpdateCompoundAgingRelaxation(int id, [FromBody] BNB_Compound_Aging_RelaxationDTO CompoundAgingRelaxation)
        {
            try
            {
                if (CompoundAgingRelaxation == null)
                {
                    _logger.LogError("CompoundAgingRelaxation object sent from client is null.");
                    return BadRequest("CompoundAgingRelaxation object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid CompoundAgingRelaxation object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var CompoundAgingRelaxationEntity = _repository.BNBCompoundAgingRelaxation.GetCompoundAgingRelaxationById(id);
                if (CompoundAgingRelaxationEntity == null)
                {
                    _logger.LogError($"CompoundAgingRelaxation with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                //CompoundAgingRelaxation.BNB_ShiftDate = DateTime.Today;
                //CompoundAgingRelaxation.Modified_By = "Admin";
                _mapper.Map(CompoundAgingRelaxation, CompoundAgingRelaxationEntity);
                _repository.BNBCompoundAgingRelaxation.UpdateCompound(CompoundAgingRelaxationEntity);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
