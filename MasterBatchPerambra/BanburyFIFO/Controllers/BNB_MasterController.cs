﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace BanburyFIFO.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BNB_MasterController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public BNB_MasterController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {

            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]

        public async Task<IEnumerable<BNB_MasterDTO>> GetAllbanburys()
        {
            try
            {
                var BNB_Master = await _repository.BNBMaster.GetAllbanburys();
                _logger.LogInfo($"Returned all Banbury from database.");
                var BNB_MasterResult = _mapper.Map<IEnumerable<BNB_MasterDTO>>(BNB_Master);
                return BNB_MasterResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllbanburys action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_MasterDTO>)StatusCode(500, "Internal server error");
            }

        }


        [HttpPost]
        public IActionResult CreateBanbury([FromBody] BNB_MasterDTO Banbury)
        {
            try
            {
                if (Banbury == null)
                {
                    _logger.LogError("Banbury object sent from client is null.");
                    return BadRequest("Banbury object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Banbury object sent from client.");
                    return BadRequest("Invalid model object");
                }


                // Banbury.Banbury_Id = new Guid();
                var BanburyEntity = _mapper.Map<BNBMaster>(Banbury);




                _repository.BNBMaster.Createbanbury(BanburyEntity);
                _repository.Save();

                var createdBanbury = _mapper.Map<BNB_MasterDTO>(BanburyEntity);
                return CreatedAtAction("GetAllbanburys", new { id = createdBanbury.BNB_ID }, createdBanbury);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateBanbury action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteBanbury(int id)
        {
            try
            {
                var Banbury = _repository.BNBMaster.GetbanburyById(id);
                if (Banbury == null)
                {
                    _logger.LogError($"Banbury with Code: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.BNBMaster.Delete(Banbury);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public IActionResult UpdateBanbury(int id, [FromBody] BNB_MasterDTO Banbury)
        {
            try
            {
                if (Banbury == null)
                {
                    _logger.LogError("Banbury object sent from client is null.");
                    return BadRequest("Banbury object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Banbury object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var BanburyEntity = _repository.BNBMaster.GetbanburyById(id);
                if (BanburyEntity == null)
                {
                    _logger.LogError($"Banbury with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(Banbury, BanburyEntity);
                _repository.BNBMaster.Updatebanbury(BanburyEntity);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
