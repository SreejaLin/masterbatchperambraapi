﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace BanburyFIFO.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BNB_Compound_MasterController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public BNB_Compound_MasterController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {

            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]

        public async Task<IEnumerable<BNB_Compound_MasterDTO>> GetAllCompound()
        {
            try
            {
                var BNB_Compound_Master = await _repository.BNBCompoundMaster.GetAllCompound();
                _logger.LogInfo($"Returned all Compound from database.");
                var BNB_Compound_MasterResult = _mapper.Map<IEnumerable<BNB_Compound_MasterDTO>>(BNB_Compound_Master);
             
                return BNB_Compound_MasterResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllCompound action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Compound_MasterDTO>)StatusCode(500, "Internal server error");
            }

        }


        [HttpPost]
        public IActionResult CreateCompound([FromBody] BNB_Compound_MasterDTO Compound)
        {
            try
            {
                if (Compound == null)
                {
                    _logger.LogError("Compound object sent from client is null.");
                    return BadRequest("Compound object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Compound object sent from client.");
                    return BadRequest("Invalid model object");
                }
                Compound.Entry_Date = DateTime.Today;
                //Compound.BNB_ShiftTime = DateTime.Now;

                // Compound.Compound_Id = new Guid();
                var CompoundEntity = _mapper.Map<BNBCompoundMaster>(Compound);

                _repository.BNBCompoundMaster.CreateCompound(CompoundEntity);
                _repository.Save();

                var createdCompound = _mapper.Map<BNB_Compound_MasterDTO>(CompoundEntity);
                return CreatedAtAction("GetAllCompound", new { id = createdCompound.Compound_TypeId }, createdCompound);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateCompound action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteCompound(int id)
        {
            try
            {
                var Compound = _repository.BNBCompoundMaster.GetCompoundById(id);
                if (Compound == null)
                {
                    _logger.LogError($"Compound with Code: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.BNBCompoundMaster.DeleteCompound(Compound);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public IActionResult UpdateCompound(int id, [FromBody] BNB_Compound_MasterDTO Compound)
        {
            try
            {
                if (Compound == null)
                {
                    _logger.LogError("Compound object sent from client is null.");
                    return BadRequest("Compound object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Compound object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var CompoundEntity = _repository.BNBCompoundMaster.GetCompoundById(id);
                if (CompoundEntity == null)
                {
                    _logger.LogError($"Compound with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                //Compound.BNB_ShiftDate = DateTime.Today;
                //Compound.Modified_By = "Admin";
                _mapper.Map(Compound, CompoundEntity);
                _repository.BNBCompoundMaster.UpdateCompound(CompoundEntity);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateCompound action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
