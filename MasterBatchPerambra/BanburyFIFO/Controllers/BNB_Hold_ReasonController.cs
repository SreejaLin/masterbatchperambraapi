﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace BanburyFIFO.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BNB_Hold_ReasonController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public BNB_Hold_ReasonController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {

            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]

        public async Task<IEnumerable<BNB_Hold_ReasonsDTO>> GetAllHoldReasons()
        {
            try
            {
                var BNB_Hold_Reasons = await _repository.BNBHoldReasons.GetAllholdreasons();
                _logger.LogInfo($"Returned all HoldReasons from database.");
                var BNB_Hold_ReasonsResult = _mapper.Map<IEnumerable<BNB_Hold_ReasonsDTO>>(BNB_Hold_Reasons);
                return BNB_Hold_ReasonsResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllHoldReasons action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Hold_ReasonsDTO>)StatusCode(500, "Internal server error");
            }

        }


        [HttpPost]
        public IActionResult CreateHoldReasons([FromBody] BNB_Hold_ReasonsDTO HoldReasons)
        {
            try
            {
                if (HoldReasons == null)
                {
                    _logger.LogError("HoldReasons object sent from client is null.");
                    return BadRequest("HoldReasons object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid HoldReasons object sent from client.");
                    return BadRequest("Invalid model object");
                }


                // HoldReasons.HoldReasons_Id = new Guid();
                var HoldReasonsEntity = _mapper.Map<BNBHoldReasons>(HoldReasons);




                _repository.BNBHoldReasons.CreateHoldReasons(HoldReasonsEntity);
                _repository.Save();

                var createdHoldReasons = _mapper.Map<BNB_Hold_ReasonsDTO>(HoldReasonsEntity);
                return CreatedAtAction("GetAllHoldReasons", new { id = createdHoldReasons.Hold_reason_id }, createdHoldReasons);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateHoldReasons action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteHoldReasons(int id)
        {
            try
            {
                var HoldReasons = _repository.BNBHoldReasons.GetHoldReasonsById(id);
                if (HoldReasons == null)
                {
                    _logger.LogError($"HoldReasons with Code: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.BNBHoldReasons.DeleteHoldReasons(HoldReasons);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public IActionResult UpdateHoldReasons(int id, [FromBody] BNB_Hold_ReasonsDTO HoldReasons)
        {
            try
            {
                if (HoldReasons == null)
                {
                    _logger.LogError("HoldReasons object sent from client is null.");
                    return BadRequest("HoldReasons object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid HoldReasons object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var HoldReasonsEntity = _repository.BNBHoldReasons.GetHoldReasonsById(id);
                if (HoldReasonsEntity == null)
                {
                    _logger.LogError($"HoldReasons with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                //HoldReasons.BNB_ShiftDate = DateTime.Today;
                //HoldReasons.Modified_By = "Admin";
                _mapper.Map(HoldReasons, HoldReasonsEntity);
                _repository.BNBHoldReasons.UpdateHoldReasons(HoldReasonsEntity);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
