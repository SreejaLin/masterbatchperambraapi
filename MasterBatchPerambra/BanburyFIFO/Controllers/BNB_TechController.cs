﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace BanburyFIFO.Controllers
{
    
        [ApiController]
        [Route("[controller]")]
        public class BNB_TechController : ControllerBase
        {
            private ILoggerManager _logger;
            private IRepositoryWrapper _repository;
            private IMapper _mapper;


            public BNB_TechController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
            {

                _logger = logger;
                _repository = repository;
                _mapper = mapper;
                _repository = repository;
            }




        [HttpGet("GetProductionEntryForTechApproval")]

        public async Task<IEnumerable<BNB_Production_EntryDTO>> GetProductionEntryForTechApproval()
        {
            try
            {
                var BNB_Production_Entry = await _repository.BNBProductionEntry.GetProductionEntryForTechApproval();
                _logger.LogInfo($"Returned all ProductionEntryPending for approval from database.");
                var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Production_Entry);
                return BNB_Production_EntryResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProductionEntryForLabApproval action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
            }

        }




            [HttpPut("{UpdateProductionEntryTechStatus}")]
            public IActionResult UpdateProductionEntryLabStatus( BNB_TechStatusDTO techstatus )
            {
                try
                {
                    if (techstatus == null)
                    {
                        _logger.LogError("Lab Status object sent from client is null.");
                        return BadRequest("Lab Status object is null");
                    }
                    if (!ModelState.IsValid)
                    {
                        _logger.LogError("Invalid Lab Status object sent from client.");
                        return BadRequest("Invalid model object");
                    }
                    var ProductionEntry = _repository.BNBProductionEntry.GetProductionEntryById(techstatus.BNB_Prod_Id);
                    if (ProductionEntry == null)
                    {
                        _logger.LogError($"ProductionEntry with id: {techstatus.BNB_Prod_Id}, hasn't been found in db.");
                        return NotFound();
                    }
                //insert into log table
                var LogStatusdObj = new BNB_LogStatusDTO();
                LogStatusdObj.Log_Id = 0;
                LogStatusdObj.Plant_code = "1001";
                LogStatusdObj.Prod_Barcode = techstatus.Prod_Barcode;
                LogStatusdObj.BNB_Prod_id = techstatus.BNB_Prod_Id;
                LogStatusdObj.Actions = techstatus.Tech_Status;

                LogStatusdObj.UserGroup = "Tech";
                LogStatusdObj.ActionTime = DateTime.Now;
                LogStatusdObj.User = techstatus.Tech_EmpCode;
                LogStatusdObj.UserRemarks = techstatus.Tech_Remarks;

                var LogStatusEntity = _mapper.Map<BNBLogStatus>(LogStatusdObj);
                _repository.BNBLogStatus.CreateLogStatus(LogStatusEntity);
           
                _repository.BNBProductionEntry.UpdateProductionEntryTechStatus(techstatus);
                _repository.Save();



                //insert into TagGeneration
                if (techstatus.Tech_Status == "DISPOSE")
                { 
                 var TagGenObj = new BNB_Tag_GenerationDTO();
                TagGenObj.TagId = 0;
                TagGenObj.Printed_DateTime = DateTime.Now;
                TagGenObj.User_Id = Convert.ToInt32(ProductionEntry.Tech_EmpCode);
                TagGenObj.Prod_Id = ProductionEntry.BNB_Prod_Id;
                TagGenObj.Module_Name = "BNBTECHOK";
                TagGenObj.Print_Status = 0;
                TagGenObj.Printer_Name = "BNBPrinterTech" ;
                TagGenObj.TagDetails =  ProductionEntry.Prod_Barcode + "|" + ProductionEntry.Compound_Code + "|" + ProductionEntry.Banbury + "|" + ProductionEntry.Skid_NO + "|" + ProductionEntry.Batch_No + "|" + ProductionEntry.Shift_Date.ToString("dd-M-yyyy HH:mm:ss") + "|" + ProductionEntry.Shift + "|" + ProductionEntry.BNB_ProdTime.ToString("dd-M-yyyy HH:mm:ss") + "|" + ProductionEntry.Operator_Code + "/" + ProductionEntry.Trucker_Code + "|" + ProductionEntry.Location + "|" + ProductionEntry.Use_Before.ToString("dd-M-yyyy HH:mm:ss") + " |" + ProductionEntry.Lab_Status + "|" + ProductionEntry.Prod_Remarks
                   + "|" + ProductionEntry.Lab_Remarks + "|" + techstatus.Tech_Status + "|" + techstatus.Tech_Remarks ;
                    var TagGenEntity = _mapper.Map<BNBTagGeneration>(TagGenObj);
                    _repository.BNBTagGeneration.CreateTagGeneration(TagGenEntity);
                    _repository.Save();
                

                }
                
                return NoContent();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                    return StatusCode(500, "Internal server error");
                }
            }



            [HttpGet("{compound}")]///{compound}")]
        public async Task<IEnumerable<BNB_Production_EntryDTO>> GetCompoundsForRelaxation(string compound)//, string time)
         {

            try
            {
                var BNB_Production_Entry = await _repository.BNBProductionEntry.GetCompoundsForRelaxation(compound);//, time);
                _logger.LogInfo($"Returned all ProductionEntryPending for approval from database.");
                var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Production_Entry);
                return BNB_Production_EntryResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProductionEntryForLabApproval action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("RelaxAgeforCompoundProdId")]
        public IActionResult RelaxAgeforCompoundProdId(BNB_TechStatusDTO techstatus)
        {
            try
            {
                if (techstatus == null)
                {
                    _logger.LogError("Lab Status object sent from client is null.");
                    return BadRequest("Lab Status object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Lab Status object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var ProductionEntry = _repository.BNBProductionEntry.GetProductionEntryById(techstatus.BNB_Prod_Id);
                if (ProductionEntry == null)
                {
                    _logger.LogError($"ProductionEntry with id: {techstatus.BNB_Prod_Id}, hasn't been found in db.");
                    return NotFound();
                }
                //insert into log table
                var LogStatusdObj = new BNB_LogStatusDTO();
                LogStatusdObj.Log_Id = 0;
                LogStatusdObj.Plant_code = "1001";
                LogStatusdObj.Prod_Barcode = techstatus.Prod_Barcode;
                LogStatusdObj.BNB_Prod_id = techstatus.BNB_Prod_Id;
                LogStatusdObj.Actions = techstatus.Tech_Status;

                LogStatusdObj.UserGroup = "Tech";
                LogStatusdObj.ActionTime = DateTime.Now;
                LogStatusdObj.User = techstatus.Tech_EmpCode;
                LogStatusdObj.UserRemarks = techstatus.Tech_Remarks;

                var LogStatusEntity = _mapper.Map<BNBLogStatus>(LogStatusdObj);
                _repository.BNBLogStatus.CreateLogStatus(LogStatusEntity);

                _repository.BNBProductionEntry.RelaxAgeforCompoundProdId(techstatus);
                _repository.Save();



              

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }



    }

}
