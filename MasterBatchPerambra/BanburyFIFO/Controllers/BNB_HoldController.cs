﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace BanburyFIFO.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BNB_HoldController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public BNB_HoldController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {

            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]

        public async Task<IEnumerable<BNB_HoldDTO>> GetAllhold()
        {
            try
            {
                var BNB_Holds = await _repository.BNBHold.GetAllHold();
                _logger.LogInfo($"Returned all hold from database.");
                var BNB_HoldsResult = _mapper.Map<IEnumerable<BNB_HoldDTO>>(BNB_Holds);
                return BNB_HoldsResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllhold action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_HoldDTO>)StatusCode(500, "Internal server error");
            }

        }


        [HttpPost]
        public IActionResult Createhold([FromBody] BNB_HoldDTO hold)
        {
            try
            {
                if (hold == null)
                {
                    _logger.LogError("hold object sent from client is null.");
                    return BadRequest("hold object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid hold object sent from client.");
                    return BadRequest("Invalid model object");
                }


                // hold.hold_Id = new Guid();
                var holdEntity = _mapper.Map<BNBHold>(hold);




                _repository.BNBHold.CreateHold(holdEntity);
                _repository.Save();

                var createdhold = _mapper.Map<BNB_HoldDTO>(holdEntity);
                return CreatedAtAction("GetAllhold", new { id = createdhold.Hold_id }, createdhold);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Createhold action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult Deletehold(int id)
        {
            try
            {
                var hold = _repository.BNBHold.GetHoldById(id);
                if (hold == null)
                {
                    _logger.LogError($"hold with Code: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.BNBHold.DeleteHold(hold);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public IActionResult Updatehold(int id, [FromBody] BNB_HoldDTO hold)
        {
            try
            {
                if (hold == null)
                {
                    _logger.LogError("hold object sent from client is null.");
                    return BadRequest("hold object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid hold object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var holdEntity = _repository.BNBHold.GetHoldById(id);
                if (holdEntity == null)
                {
                    _logger.LogError($"hold with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                //hold.BNB_ShiftDate = DateTime.Today;
                //hold.Modified_By = "Admin";
                _mapper.Map(hold, holdEntity);
                _repository.BNBHold.UpdateHold(holdEntity);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
