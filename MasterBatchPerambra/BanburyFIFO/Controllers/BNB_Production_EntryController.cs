using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;
using System.Globalization;

namespace BanburyFIFO.Controllers
{

        [ApiController]
        [Route("[controller]")]
        public class BNB_Production_EntryController : ControllerBase
        {
            private ILoggerManager _logger;
            private IRepositoryWrapper _repository;
            private IMapper _mapper;


            public BNB_Production_EntryController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
            {

                _logger = logger;
                _repository = repository;
                _mapper = mapper;
                _repository = repository;
            }

            [HttpGet]

            public async Task<IEnumerable<BNB_Production_EntryDTO>> GetAllProductionEntry()
            {
                try
                {
                    var BNB_Production_Entry = await _repository.BNBProductionEntry.GetAllProductionEntry();
                    _logger.LogInfo($"Returned all ProductionEntry from database.");
                    var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Production_Entry);
                    return BNB_Production_EntryResult;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside GetAllProductionEntry action: {ex.Message}");
                    //return new StatusCode { Ok = false };
                    return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
                }

            }



        [HttpGet("GetProductionEntryForLabApproval")]

        public async Task<IEnumerable<BNB_Production_EntryDTO>> GetProductionEntryForLabApproval()
        {
            try
            {
                var BNB_Production_Entry = await _repository.BNBProductionEntry.GetProductionEntryForLabApproval();
                _logger.LogInfo($"Returned all ProductionEntryPending for approval from database.");
                var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Production_Entry);
                return BNB_Production_EntryResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProductionEntryForLabApproval action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
            }

        }

        [HttpGet("GetFiveLocationforcompounds/{compound_Code}")]

        public async Task<IEnumerable<BNB_Production_EntryDTO>> GetFiveLocationforcompounds(string compound_Code)
        {
            try
            {

                //var mappedcompound =  _repository.BNBMaterialMapping.GetMaterialMapping(compound_Code);

                var BNB_Production_Entry = await _repository.BNBProductionEntry.GetFiveLocationforcompounds(compound_Code );
                _logger.LogInfo($"Returned all ProductionEntryPending for approval from database.");
                var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Production_Entry);
                return BNB_Production_EntryResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetFiveLocationforcompounds action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
            }

        }


        [HttpPost]
            public IActionResult CreateProductionEntry([FromBody] BNB_Production_EntryDTO ProductionEntry)
            {
                try
                {
                    if (ProductionEntry == null)
                    {
                        _logger.LogError("ProductionEntry object sent from client is null.");
                        return BadRequest("ProductionEntry object is null");
                    }
                    if (!ModelState.IsValid)
                    {
                        _logger.LogError("Invalid ProductionEntry object sent from client.");
                        return BadRequest("Invalid model object");
                    }



                   ProductionEntry.Shift_Date = Convert.ToDateTime(ProductionEntry.bNBShiftDate);
                if(ProductionEntry.Shift == "C")
                {
                    DateTime ShiftA, NextDayStartTime,now;
                    ShiftA = Convert.ToDateTime("06:01:00");
                    NextDayStartTime = Convert.ToDateTime("00:01:00");
                   // string cnow = "00:04:00";
                     now = DateTime.Now;
                    //now = Convert.ToDateTime(cnow);

                    if(now > NextDayStartTime  && now< ShiftA)
                    {
                        ProductionEntry.Shift_Date = ProductionEntry.Shift_Date.AddDays(-1);
                    }
                 }

                bool exits = _repository.BNBProductionEntry.CheckAlreadyExisting(ProductionEntry.Compound_Code, ProductionEntry.Skid_NO,ProductionEntry.Shift_Date,ProductionEntry.Shift, ProductionEntry.Specail_Trails);
                if(exits == true)
                {
                    return Ok("Already Exists");
                }
                //check location status

                bool lococcupied = _repository.BNBLocationMaster.GetlocationStatusByName(ProductionEntry.Location);
                if(!lococcupied)
                {
                    return Ok("Please select another location");
                }

                //  ProductionEntry.Trucker_Code = "222222";
                if(ProductionEntry.BNB_ProdTime == Convert.ToDateTime("01 - 01 - 0001 00:00:00"))
                {
                    ProductionEntry.BNB_ProdTime = DateTime.Now;
                    ProductionEntry.Ageing_Time = ProductionEntry.BNB_ProdTime.AddHours(4);
                    ProductionEntry.Use_Before = _repository.BNBProductionEntry.GetUseBeforeDate();
                }
                else
                {

                    var bnbPrddate = ProductionEntry.BNB_ProdTime.ToString("MM-dd-yyyy HH:mm:ss");
                    ProductionEntry.BNB_ProdTime = Convert.ToDateTime( bnbPrddate);
                    ProductionEntry.Ageing_Time = ProductionEntry.BNB_ProdTime.AddHours(4);
                    ProductionEntry.Use_Before = _repository.BNBProductionEntry.GetUseBeforeDateprdate(Convert.ToDateTime(bnbPrddate));
                }


                    ProductionEntry.No_OF_Batches = (Convert.ToInt32(ProductionEntry.BatchII) - Convert.ToInt32(ProductionEntry.BatchI)) + 1;
                    ProductionEntry.Prod_Status = ProductionEntry.Prod_Status.ToUpper();

                   //generate bar code
                    ProductionEntry.Prod_Barcode = _repository.BNBProductionEntry.GetBarcodeForProduction(ProductionEntry.Compound_Code);
                 if (ProductionEntry.Prod_Status == "OK")
                 {

                    ProductionEntry.Prod_Remarks = "";
                 }

                var ProductionEntryEntity = _mapper.Map<BNBProductionEntry>(ProductionEntry);

                   _repository.BNBProductionEntry.CreateProductionEntry(ProductionEntryEntity);
                    _repository.Save();
                    var createdProductionEntry = _mapper.Map<BNB_Production_EntryDTO>(ProductionEntryEntity);

                //insert into TagGeneration

                    var TagGenObj = new BNB_Tag_GenerationDTO();
                    TagGenObj.TagId = 0;
                    TagGenObj.Printed_DateTime = DateTime.Now;
                    TagGenObj.User_Id = Convert.ToInt32(ProductionEntry.Trucker_Code);
                    TagGenObj.Prod_Id = createdProductionEntry.BNB_Prod_Id;
                    TagGenObj.Module_Name = ProductionEntry.Module_Name;
                    TagGenObj.Print_Status = 0;
                    TagGenObj.Printer_Name = "BNBPrinter" + ProductionEntry.Banbury;
                    TagGenObj.TagDetails = ProductionEntry.Prod_Barcode + "|" + ProductionEntry.Compound_Code + "|" + ProductionEntry.Banbury + "|" + ProductionEntry.Skid_NO + "|" + ProductionEntry.Batch_No + "|" + ProductionEntry.Shift_Date.ToString("dd-MM-yyyy HH:mm:ss") + " : " + ProductionEntry.Shift + "|" + ProductionEntry.BNB_ProdTime.ToString("dd-MM-yyyy HH:mm:ss") + "|" + ProductionEntry.Operator_Code +"/"+ ProductionEntry.Trucker_Code + "|" + ProductionEntry.Location + "|" + ProductionEntry.Use_Before.ToString("dd-MM-yyyy HH:mm:ss") + "|" ;


                //insert into Holdtable

                if (ProductionEntry.Prod_Status == "HOLD")
                {
                    TagGenObj.TagDetails += "|" + ProductionEntry.Hold_reason;
                    ProductionEntry.Hold_reason = "";
                }

                //insert into log table
                var LogStatusdObj = new BNB_LogStatusDTO();
                LogStatusdObj.Log_Id = 0;
                LogStatusdObj.Plant_code = "1001";
                LogStatusdObj.Prod_Barcode = ProductionEntry.Prod_Barcode;
                LogStatusdObj.BNB_Prod_id = createdProductionEntry.BNB_Prod_Id;
                LogStatusdObj.Actions = ProductionEntry.Module_Name;

                LogStatusdObj.UserGroup = "Trucker";
                LogStatusdObj.ActionTime =  DateTime.Now;
                LogStatusdObj.User = ProductionEntry.Trucker_Code;
                LogStatusdObj.UserRemarks = ProductionEntry.Hold_reason;

                var LogStatusEntity = _mapper.Map<BNBLogStatus>(LogStatusdObj);
                _repository.BNBLogStatus.CreateLogStatus(LogStatusEntity);
                _repository.Save();

                //update location status
                //var LocObj = new BNB_Location_MasterDTO();
                //LocObj.BNB_Location_Name = ProductionEntry.Location;
                //var locEntity = _mapper.Map<BNBLocationMaster>(LocObj);
                _repository.BNBLocationMaster.UpdatelocationByName(ProductionEntry.Location);

                var TagGenEntity = _mapper.Map<BNBTagGeneration>(TagGenObj);
                _repository.BNBTagGeneration.CreateTagGeneration(TagGenEntity);
                _repository.Save();



                return CreatedAtAction("GetAllProductionEntry", new { id = createdProductionEntry.BNB_Prod_Id }, createdProductionEntry);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside CreateProductionEntry action: {ex.Message}");
                    return StatusCode(500, "Internal server error");
                }
            }


            [HttpDelete("{id}")]
            public IActionResult DeleteProductionEntry(int id)
            {
                try
                {
                    var ProductionEntry = _repository.BNBProductionEntry.GetProductionEntryById(id);
                    if (ProductionEntry == null)
                    {
                        _logger.LogError($"ProductionEntry with Code: {id}, hasn't been found in db.");
                        return NotFound();
                    }
                    _repository.BNBProductionEntry.DeleteProductionEntry(ProductionEntry);
                    _repository.Save();
                    return NoContent();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                    return StatusCode(500, "Internal server error");
                }
            }


            [HttpPut("{UpdateProductionEntryLabStatus}")]
            public IActionResult UpdateProductionEntryLabStatus( BNB_LabStatusDTO labstatus )
            {
                try
                {
                    if (labstatus == null)
                    {
                        _logger.LogError("Lab Status object sent from client is null.");
                        return BadRequest("Lab Status object is null");
                    }
                    if (!ModelState.IsValid)
                    {
                        _logger.LogError("Invalid Lab Status object sent from client.");
                        return BadRequest("Invalid model object");
                    }
                    var ProductionEntryEntity = _repository.BNBProductionEntry.GetProductionEntryById(labstatus.BNB_Prod_Id);
                    if (ProductionEntryEntity == null)
                    {
                        _logger.LogError($"ProductionEntry with id: {labstatus.BNB_Prod_Id}, hasn't been found in db.");
                        return NotFound();
                    }
                //insert into log table
                var LogStatusdObj = new BNB_LogStatusDTO();
                LogStatusdObj.Log_Id = 0;
                LogStatusdObj.Plant_code = "1001";
                LogStatusdObj.Prod_Barcode = labstatus.Prod_Barcode;
                LogStatusdObj.BNB_Prod_id = labstatus.BNB_Prod_Id;
                LogStatusdObj.Actions = labstatus.Lab_Status;

                LogStatusdObj.UserGroup = "Lab";
                LogStatusdObj.ActionTime = DateTime.Now;
                LogStatusdObj.User = labstatus.Lab_EmpCode;
                LogStatusdObj.UserRemarks = labstatus.Lab_Remarks;

                var LogStatusEntity = _mapper.Map<BNBLogStatus>(LogStatusdObj);
                _repository.BNBLogStatus.CreateLogStatus(LogStatusEntity);
                _repository.Save();
                _repository.BNBProductionEntry.UpdateProductionEntryLabStatus(labstatus);
                    _repository.Save();
                    return NoContent();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                    return StatusCode(500, "Internal server error");
                }
            }

        [HttpPut("UpdateConsumeptionStatus")]
        public IActionResult UpdateConsumeptionStatus(BNB_ConsumptionDTO bNB_Consump)
        {
            try
            {
                if (bNB_Consump == null)
                {
                    _logger.LogError("Lab Status object sent from client is null.");
                    return BadRequest("Lab Status object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Lab Status object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var ProductionEntryEntity = _repository.BNBProductionEntry.GetProductionEntryById(bNB_Consump.bnB_Prod_Id);
                if (ProductionEntryEntity == null)
                {
                    _logger.LogError($"ProductionEntry with id: {bNB_Consump.bnB_Prod_Id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.BNBProductionEntry.UpdateConsumeptionStatus(bNB_Consump);
                _repository.Save();
                //insert into log table
                var LogStatusdObj = new BNB_LogStatusDTO();
                LogStatusdObj.Log_Id = 0;
                LogStatusdObj.Plant_code = "1001";
                LogStatusdObj.Prod_Barcode = ProductionEntryEntity.Prod_Barcode;
                LogStatusdObj.BNB_Prod_id = ProductionEntryEntity.BNB_Prod_Id;
                LogStatusdObj.Actions = "OK";

                LogStatusdObj.UserGroup = "FORKLIFTER";
                LogStatusdObj.ActionTime = DateTime.Now;
                LogStatusdObj.User = bNB_Consump.ForkliftOpt_Code;
                LogStatusdObj.UserRemarks = "CONSUMED";

                var LogStatusEntity = _mapper.Map<BNBLogStatus>(LogStatusdObj);
                _repository.BNBLogStatus.CreateLogStatus(LogStatusEntity);
                _repository.BNBLocationMaster.UpdatelocationByName(ProductionEntryEntity.Location);
                //var LogStatusdObj = new BNB_LogStatusDTO();
                //LogStatusdObj.Log_Id = 0;
                //LogStatusdObj.Plant_code = "1001";
                //LogStatusdObj.Prod_Barcode = bnB_Prod_Id.Prod_Barcode;
                //LogStatusdObj.BNB_Prod_id = bnB_Prod_Id.BNB_Prod_Id;
                //LogStatusdObj.Actions = bnB_Prod_Id.Lab_Status;

                //LogStatusdObj.UserGroup = "Lab";
                //LogStatusdObj.ActionTime = DateTime.Now;
                //LogStatusdObj.User = bnB_Prod_Id.Lab_EmpCode;
                //LogStatusdObj.UserRemarks = bnB_Prod_Id.Lab_Remarks;

                //var LogStatusEntity = _mapper.Map<BNBLogStatus>(LogStatusdObj);
                //_repository.BNBLogStatus.CreateLogStatus(LogStatusEntity);
                //_repository.Save();
                _repository.BNBProductionEntry.UpdateConsumeptionStatus(bNB_Consump);

                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpGet("TagRePrintfortrucker/{Shift_Date}/{shift}")]
        public async Task<IEnumerable<BNB_Production_EntryDTO>> TagRePrintfortrucker(string Shift_Date, string shift)
        {
            try
            {
                var BNB_Production_Entry = await _repository.BNBProductionEntry.TagRePrintfortrucker(Shift_Date, shift);
                _logger.LogInfo($"Returned all ProductionEntryPending for approval from database.");
                var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Production_Entry);
                return BNB_Production_EntryResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetFiveLocationforcompounds action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
            }

        }
        //to get so schedule
        [HttpGet("GetscheduleForTagPrintforSO/{Shift_Date}/{shift}/{bnb}")]
        public async Task<IActionResult> GetscheduleForTagPrintforSO(string Shift_Date, string shift, string bnb)
        {
            try
            {
                var BNB_PPC =  _repository.BNBProductionEntry.GetPPCScheduleForDates(Shift_Date, shift,bnb);
                if (BNB_PPC == null)
                {

                    return NotFound();
                }


                return Ok(BNB_PPC);

            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

    }


        [HttpGet("{bnb}/{shift}")]
            public async Task<IActionResult> GetscheduleForBNB_Shift(string bnb, string shift)
            {

                try
                {
                    var ifexist = _repository.BNBProductionEntry.GetPPCScheduleForBNB(bnb, shift);
                    if (ifexist == null)
                    {

                        return NotFound();
                    }

                foreach (var item in ifexist)
                {
                    item.Mapped_Comp_Code = _repository.BNBMaterialMapping.GetMaterialMapping(item.Comp_Code);
                }
                return Ok(ifexist);

                }

                catch (Exception ex)
                {
                    //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                    return StatusCode(500, "Internal server error");
                }
            }

        }
 }
