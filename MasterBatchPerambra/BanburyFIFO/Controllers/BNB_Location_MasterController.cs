﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;


namespace BanburyFIFO.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BNB_Location_MasterController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public BNB_Location_MasterController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {

            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _repository = repository;
        }
        [HttpGet]

        public async Task<IEnumerable<BNB_Location_MasterDTO>> GetAllLocation()
        {
            try
            {
                var BNB_Location = await _repository.BNBLocationMaster.GetAlllocations();
                _logger.LogInfo($"Returned all Location from database.");
                var BNB_LocationResult = _mapper.Map<IEnumerable<BNB_Location_MasterDTO>>(BNB_Location);
                return BNB_LocationResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllLocation action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Location_MasterDTO>)StatusCode(500, "Internal server error");
            }

        }
        [HttpGet("{locationname}")]

        public async Task<IEnumerable<BNB_Location_MasterDTO>> GetLocationStatus(string locationname)
        {
            try
            {
                var BNB_Location = await _repository.BNBLocationMaster.GetAlllocations();
                _logger.LogInfo($"Returned all Location from database.");
                var BNB_LocationResult = _mapper.Map<IEnumerable<BNB_Location_MasterDTO>>(BNB_Location);
                return BNB_LocationResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllLocation action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Location_MasterDTO>)StatusCode(500, "Internal server error");
            }

        }

        [HttpPost]
        public IActionResult CreateLocation([FromBody] BNB_Location_MasterDTO Location)
        {
            try
            {
                if (Location == null)
                {
                    _logger.LogError("Location object sent from client is null.");
                    return BadRequest("Location object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Location object sent from client.");
                    return BadRequest("Invalid model object");
                }


                // Location.Location_Id = new Guid();
                var LocationEntity = _mapper.Map<BNBLocationMaster>(Location);




                _repository.BNBLocationMaster.Createlocation(LocationEntity);
                _repository.Save();

                var createdLocation = _mapper.Map<BNB_Location_MasterDTO>(LocationEntity);
                return CreatedAtAction("GetAllLocation", new { id = createdLocation.BNB_Location_id }, createdLocation);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateLocation action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteLocation(int id)
        {
            try
            {
                var Location = _repository.BNBLocationMaster.GetlocationById(id);
                if (Location == null)
                {
                    _logger.LogError($"Location with Code: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.BNBLocationMaster.Deletelocation(Location);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public IActionResult UpdateLocation(int id, [FromBody] BNB_Location_MasterDTO Location)
        {
            try
            {
                if (Location == null)
                {
                    _logger.LogError("Location object sent from client is null.");
                    return BadRequest("Location object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Location object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var LocationEntity = _repository.BNBLocationMaster.GetlocationById(id);
                if (LocationEntity == null)
                {
                    _logger.LogError($"Location with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                //Location.BNB_ShiftDate = DateTime.Today;
                //Location.Modified_By = "Admin";
                _mapper.Map(Location, LocationEntity);
                _repository.BNBLocationMaster.Updatelocation(LocationEntity);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }




    }
}
