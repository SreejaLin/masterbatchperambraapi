﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace BanburyFIFO.Controllers
{

        [ApiController]
        [Route("[controller]")]
        public class BNB_ReportsController : ControllerBase
        {
            private ILoggerManager _logger;
            private IRepositoryWrapper _repository;
            private IMapper _mapper;


            public BNB_ReportsController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
            {

                _logger = logger;
                _repository = repository;
                _mapper = mapper;
                _repository = repository;
            }
        /// <summary>
        /// report generatiopn
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        [HttpGet("GetAllProductionReport/{FromDate}/{ToDate}")]

        public async Task<IEnumerable<BNB_Production_EntryDTO>> GetAllProductionReport(DateTime FromDate, DateTime ToDate)
            {
                try
                {
                    var BNB_Production_Entry = await _repository.Reports.GetAllProductionReport(FromDate,ToDate);
                    _logger.LogInfo($"Returned all ProductionReport from database.");
                    var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Production_Entry);
                    return BNB_Production_EntryResult;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside GetAllProductionEntry action: {ex.Message}");
                    //return new StatusCode { Ok = false };
                    return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
                }

            }


        [HttpGet("GetAllConsumptionReport/{FromDate}/{ToDate}")]
        public async Task<IEnumerable<BNB_Production_EntryDTO>> GetAllConsumptionReport(DateTime FromDate, DateTime ToDate)
        {
            try
            {
                var BNB_Consumption_Entry = await _repository.Reports.GetAllConsumptionReport(FromDate, ToDate);
                _logger.LogInfo($"Returned all ProductionReport from database.");
                var BNB_Consumption_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Consumption_Entry);
                return BNB_Consumption_EntryResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllConsumptionEntry action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
            }

        }
        [HttpGet("GetAllCurrentstockReport")]
        public async Task<IEnumerable<BNB_Production_EntryDTO>> GetAllCurrentstockReport()
        {
            try
            {
                var BNB_Currentstock_Entry = await _repository.Reports.GetAllCurrentstockReport();
                _logger.LogInfo($"Returned all GetAllCurrentstockReport from database.");
                var BNB_Currentstock_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Currentstock_Entry);
                return BNB_Currentstock_EntryResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllCurrentstockEntry action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
            }

        }
        [HttpGet("GetAllOveragedReport")]
        public async Task<IEnumerable<BNB_Production_EntryDTO>> GetAllOveragedReport()
        {
            try
            {
                var BNB_Overaged_Entry = await _repository.Reports.GetAllOveragedReport();
                _logger.LogInfo($"Returned all ProductionReport from database.");
                var BNB_Overaged_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Overaged_Entry);
                return BNB_Overaged_EntryResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllOveragedEntry action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
            }

        }

    }

 }
