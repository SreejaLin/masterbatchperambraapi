﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace BanburyFIFO.Controllers
{
    
        [ApiController]
        [Route("[controller]")]
        public class BNB_RMSCompoundEntryController : ControllerBase
        {
            private ILoggerManager _logger;
            private IRepositoryWrapper _repository;
            private IMapper _mapper;


            public BNB_RMSCompoundEntryController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
            {

                _logger = logger;
                _repository = repository;
                _mapper = mapper;
                _repository = repository;
            }

            [HttpGet]

            public async Task<IEnumerable<BNB_RmsDTO>> GetAllRMSCompoundEntry()
            {
                try
                {
                    var BNB_RMSCompound_Entry = await _repository.BNBProductionEntry.GetAllProductionEntry();
                    _logger.LogInfo($"Returned all RMSCompoundEntry from database.");
                    var BNB_RMSCompound_EntryResult = _mapper.Map<IEnumerable<BNB_RmsDTO>>(BNB_RMSCompound_Entry);
                    return BNB_RMSCompound_EntryResult;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside GetAllRMSCompoundEntry action: {ex.Message}");
                    //return new StatusCode { Ok = false };
                    return (IEnumerable<BNB_RmsDTO>)StatusCode(500, "Internal server error");
                }

            }


        [HttpPost]
            public IActionResult CreateRMSCompoundEntry([FromBody] BNB_RmsDTO RMSCompoundEntry)
            {
                try
                {
                    if (RMSCompoundEntry == null)
                    {
                        _logger.LogError("RMSCompoundEntry object sent from client is null.");
                        return BadRequest("RMSCompoundEntry object is null");
                    }
                    if (!ModelState.IsValid)
                    {
                        _logger.LogError("Invalid RMSCompoundEntry object sent from client.");
                        return BadRequest("Invalid model object");
                    }

                RMSCompoundEntry.Use_Before = _repository.BNBProductionEntry.GetUseBeforeDate();
                RMSCompoundEntry.Shift_Date = Convert.ToDateTime(RMSCompoundEntry.bNBShiftDate);
                bool exits = _repository.BNBProductionEntry.CheckAlreadyExisting(RMSCompoundEntry.Compound_Code, RMSCompoundEntry.Skid_NO, RMSCompoundEntry.Shift_Date, RMSCompoundEntry.Shift, RMSCompoundEntry.Specail_Trails);
                if (exits == true)
                {
                    return Ok("Already Exists");
                }
                //check location status

                bool lococcupied = _repository.BNBLocationMaster.GetlocationStatusByName(RMSCompoundEntry.Location);
                if (!lococcupied)
                {
                    return Ok("Please select another location");
                }
                //RMSCompoundEntry.RMS_Crew = "234324";

                RMSCompoundEntry.BNB_ProdTime = DateTime.Now;
                    RMSCompoundEntry.Ageing_Time = RMSCompoundEntry.BNB_ProdTime.AddHours(4);
                    //RMSCompoundEntry.Use_Before = RMSCompoundEntry.BNB_ProdTime.get(10);
                    RMSCompoundEntry.No_OF_Batches = (Convert.ToInt32(RMSCompoundEntry.BatchII) - Convert.ToInt32(RMSCompoundEntry.BatchI)) + 1;
                    RMSCompoundEntry.Plant_Code = "1001";
                RMSCompoundEntry.LabStatus = "OK";
                   //generate barcode
                    RMSCompoundEntry.Prod_Barcode = _repository.BNBProductionEntry.GetBarcodeForProduction(RMSCompoundEntry.Compound_Code);
                RMSCompoundEntry.Prod_Status = "OK";
                 

                var RMSCompoundEntryEntity = _mapper.Map<BNBProductionEntry>(RMSCompoundEntry);
                                                  
                    _repository.BNBProductionEntry.CreateProductionEntry(RMSCompoundEntryEntity);
                    _repository.Save();
                    var createdRMSCompoundEntry = _mapper.Map<BNB_RmsDTO>(RMSCompoundEntryEntity);

                //insert into TagGeneration

                    var TagGenObj = new BNB_Tag_GenerationDTO();
                    TagGenObj.TagId = 0;
                    TagGenObj.Printed_DateTime = DateTime.Now;
                    TagGenObj.User_Id = Convert.ToInt32(RMSCompoundEntry.RMS_Crew);
                    TagGenObj.Prod_Id = createdRMSCompoundEntry.BNB_Prod_Id;
                    TagGenObj.Module_Name = RMSCompoundEntry.Module_Name;
                    TagGenObj.Print_Status = 0;
                    TagGenObj.Printer_Name = "BNBPrinterRMS" ;
                    TagGenObj.TagDetails = RMSCompoundEntry.Prod_Barcode + "|" + RMSCompoundEntry.Compound_Code + "|" + RMSCompoundEntry.Banbury + "|" + RMSCompoundEntry.Skid_NO + "|" + RMSCompoundEntry.Batch_No + "|" + RMSCompoundEntry.Shift_Date.ToString("dd-M-yyyy HH:mm:ss") + "|" + RMSCompoundEntry.Shift + "|" + RMSCompoundEntry.BNB_ProdTime.ToString("dd-M-yyyy HH:mm:ss") + "|" + RMSCompoundEntry.Vendor +"/"+ RMSCompoundEntry.RMS_Crew + "|" + RMSCompoundEntry.Location + "|" + RMSCompoundEntry.Use_Before.ToString("dd-M-yyyy HH:mm:ss") + "|" ;


                
                //insert into log table
                var LogStatusdObj = new BNB_LogStatusDTO();
                LogStatusdObj.Log_Id = 0;
                LogStatusdObj.Plant_code = "1001";
                LogStatusdObj.Prod_Barcode = RMSCompoundEntry.Prod_Barcode;
                LogStatusdObj.BNB_Prod_id = createdRMSCompoundEntry.BNB_Prod_Id;
                LogStatusdObj.Actions = RMSCompoundEntry.Module_Name;

                LogStatusdObj.UserGroup = "RMS";
                LogStatusdObj.ActionTime = DateTime.Now;
                LogStatusdObj.User = RMSCompoundEntry.RMS_Crew;
               

                var LogStatusEntity = _mapper.Map<BNBLogStatus>(LogStatusdObj);
                _repository.BNBLogStatus.CreateLogStatus(LogStatusEntity);
                _repository.Save();

                //update location status
                var LocObj = new BNB_Location_MasterDTO();
                LocObj.BNB_Location_Name = RMSCompoundEntry.Location;
                var locEntity = _mapper.Map<BNBLocationMaster>(LocObj);
                _repository.BNBLocationMaster.UpdatelocationByName(RMSCompoundEntry.Location);

                var TagGenEntity = _mapper.Map<BNBTagGeneration>(TagGenObj);
                _repository.BNBTagGeneration.CreateTagGeneration(TagGenEntity);
                _repository.Save();



                return CreatedAtAction("GetAllRMSCompoundEntry", new { id = createdRMSCompoundEntry.BNB_Prod_Id }, createdRMSCompoundEntry);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside CreateRMSCompoundEntry action: {ex.Message}");
                    return StatusCode(500, "Internal server error");
                }
            }


            [HttpDelete("{id}")]
            public IActionResult DeleteRMSCompoundEntry(int id)
            {
                try
                {
                    var RMSCompoundEntry = _repository.BNBProductionEntry.GetProductionEntryById(id);
                    if (RMSCompoundEntry == null)
                    {
                        _logger.LogError($"RMSCompoundEntry with Code: {id}, hasn't been found in db.");
                        return NotFound();
                    }
                    _repository.BNBProductionEntry.DeleteProductionEntry(RMSCompoundEntry);
                    _repository.Save();
                    return NoContent();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                    return StatusCode(500, "Internal server error");
                }
            }


            [HttpPut("{UpdateRMSCompoundEntryLabStatus}")]
            public IActionResult UpdateRMSCompoundEntryLabStatus( BNB_LabStatusDTO labstatus )
            {
                try
                {
                    if (labstatus == null)
                    {
                        _logger.LogError("Lab Status object sent from client is null.");
                        return BadRequest("Lab Status object is null");
                    }
                    if (!ModelState.IsValid)
                    {
                        _logger.LogError("Invalid Lab Status object sent from client.");
                        return BadRequest("Invalid model object");
                    }
                    var RMSCompoundEntryEntity = _repository.BNBProductionEntry.GetProductionEntryById(labstatus.BNB_Prod_Id);
                    if (RMSCompoundEntryEntity == null)
                    {
                        _logger.LogError($"RMSCompoundEntry with id: {labstatus.BNB_Prod_Id}, hasn't been found in db.");
                        return NotFound();
                    }
                //insert into log table
                var LogStatusdObj = new BNB_LogStatusDTO();
                LogStatusdObj.Log_Id = 0;
                LogStatusdObj.Plant_code = "1001";
                LogStatusdObj.Prod_Barcode = labstatus.Prod_Barcode;
                LogStatusdObj.BNB_Prod_id = labstatus.BNB_Prod_Id;
                LogStatusdObj.Actions = labstatus.Lab_Status;

                LogStatusdObj.UserGroup = "Lab";
                LogStatusdObj.ActionTime = DateTime.Now;
                LogStatusdObj.User = labstatus.Lab_EmpCode;
                LogStatusdObj.UserRemarks = labstatus.Lab_Remarks;

                var LogStatusEntity = _mapper.Map<BNBLogStatus>(LogStatusdObj);
                _repository.BNBLogStatus.CreateLogStatus(LogStatusEntity);
                _repository.Save();
                _repository.BNBProductionEntry.UpdateProductionEntryLabStatus(labstatus);
                    _repository.Save();
                    return NoContent();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                    return StatusCode(500, "Internal server error");
                }
            }

        [HttpGet("supplier")]

        public async Task<IEnumerable<BNB_SupplierDTO>> GetAllSupplier()
        {
            try
            {
                var BNB_RMSSupplier = await _repository.BNBSupplier.GetAllSupplier();
                _logger.LogInfo($"Returned all RMSCompoundEntry from database.");
                var BNB_RMSSupplierResult = _mapper.Map<IEnumerable<BNB_SupplierDTO>>(BNB_RMSSupplier);
                return BNB_RMSSupplierResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllRMSCompoundEntry action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_SupplierDTO>)StatusCode(500, "Internal server error");
            }

        }
       

    }

}
