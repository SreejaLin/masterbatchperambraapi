﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace BanburyFIFO.Controllers
{
    
        [ApiController]
        [Route("[controller]")]
        public class BNB_LabController : ControllerBase
        {
            private ILoggerManager _logger;
            private IRepositoryWrapper _repository;
            private IMapper _mapper;


            public BNB_LabController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
            {

                _logger = logger;
                _repository = repository;
                _mapper = mapper;
                _repository = repository;
            }

            [HttpGet]

            public async Task<IEnumerable<BNB_Production_EntryDTO>> GetAllProductionEntry()
            {
                try
                {
                    var BNB_Production_Entry = await _repository.BNBProductionEntry.GetAllProductionEntry();
                    _logger.LogInfo($"Returned all ProductionEntry from database.");
                    var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Production_Entry);
                    return BNB_Production_EntryResult;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside GetAllProductionEntry action: {ex.Message}");
                    //return new StatusCode { Ok = false };
                    return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
                }

            }



        [HttpGet("GetProductionEntryForLabApproval")]

        public async Task<IEnumerable<BNB_Production_EntryDTO>> GetProductionEntryForLabApproval()
        {
            try
            {
                var BNB_Production_Entry = await _repository.BNBProductionEntry.GetProductionEntryForLabApproval();
                _logger.LogInfo($"Returned all ProductionEntryPending for approval from database.");
                var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(BNB_Production_Entry);
                return BNB_Production_EntryResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProductionEntryForLabApproval action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
            }

        }




            [HttpPut("{UpdateProductionEntryLabStatus}")]
            public IActionResult UpdateProductionEntryLabStatus( BNB_LabStatusDTO labstatus )
            {
                try
                {
                    if (labstatus == null)
                    {
                        _logger.LogError("Lab Status object sent from client is null.");
                        return BadRequest("Lab Status object is null");
                    }
                    if (!ModelState.IsValid)
                    {
                        _logger.LogError("Invalid Lab Status object sent from client.");
                        return BadRequest("Invalid model object");
                    }
                    var ProductionEntryEntity = _repository.BNBProductionEntry.GetProductionEntryById(labstatus.BNB_Prod_Id);
                    if (ProductionEntryEntity == null)
                    {
                        _logger.LogError($"ProductionEntry with id: {labstatus.BNB_Prod_Id}, hasn't been found in db.");
                        return NotFound();
                    }
                //insert into log table
                var LogStatusdObj = new BNB_LogStatusDTO();
                LogStatusdObj.Log_Id = 0;
                LogStatusdObj.Plant_code = "1001";
                LogStatusdObj.Prod_Barcode = labstatus.Prod_Barcode;
                LogStatusdObj.BNB_Prod_id = labstatus.BNB_Prod_Id;
                LogStatusdObj.Actions = labstatus.Lab_Status;

                LogStatusdObj.UserGroup = "Lab";
                LogStatusdObj.ActionTime = DateTime.Now;
                LogStatusdObj.User = labstatus.Lab_EmpCode;
                LogStatusdObj.UserRemarks = labstatus.Lab_Remarks;

                var LogStatusEntity = _mapper.Map<BNBLogStatus>(LogStatusdObj);
                _repository.BNBLogStatus.CreateLogStatus(LogStatusEntity);
                _repository.Save();
                _repository.BNBProductionEntry.UpdateProductionEntryLabStatus(labstatus);
                    _repository.Save();
                    return NoContent();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                    return StatusCode(500, "Internal server error");
                }
            }





        [HttpPut("EditProductionEntryLabStatus")]
        public IActionResult EditProductionEntryLabStatus(BNB_LabStatusDTO labstatus)
        {
            try
            {
                if (labstatus == null)
                {
                    _logger.LogError("Lab Status object sent from client is null.");
                    return BadRequest("Lab Status object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Lab Status object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var ProductionEntryEntity = _repository.BNBProductionEntry.GetProductionEntryById(labstatus.BNB_Prod_Id);
                if (ProductionEntryEntity == null)
                {
                    _logger.LogError($"ProductionEntry with id: {labstatus.BNB_Prod_Id}, hasn't been found in db.");
                    return NotFound();
                }
                //insert into log table
                var LogStatusdObj = new BNB_LogStatusDTO();
                LogStatusdObj.Log_Id = 0;
                LogStatusdObj.Plant_code = "1001";
                LogStatusdObj.Prod_Barcode = labstatus.Prod_Barcode;
                LogStatusdObj.BNB_Prod_id = labstatus.BNB_Prod_Id;
                LogStatusdObj.Actions = labstatus.Lab_Status +" Edited";

                LogStatusdObj.UserGroup = "Lab";
                LogStatusdObj.ActionTime = DateTime.Now;
                LogStatusdObj.User = labstatus.Lab_EmpCode;
                LogStatusdObj.UserRemarks = labstatus.Lab_Remarks;

                var LogStatusEntity = _mapper.Map<BNBLogStatus>(LogStatusdObj);
                _repository.BNBLogStatus.CreateLogStatus(LogStatusEntity);
                _repository.Save();
                _repository.BNBProductionEntry.EditProductionEntryLabStatus(labstatus);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }






        [HttpGet("{bnb}/{shift}/{date}")]///{compound}")]
        public async Task<IEnumerable<BNB_Production_EntryDTO>> GetUpdatedProdEntryForDateCompondShiftBNB(string bnb, string shift,DateTime date)//, string compound)
            {

                try
                {
                var ifexist = await _repository.BNBProductionEntry.GetUpdatedProdEntryForDateCompondShiftBNB(bnb, shift, date);//, compound);
                  _logger.LogInfo($"Returned all ProductionEntry Updated by Lab from database.");
                var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_Production_EntryDTO>>(ifexist);
                return BNB_Production_EntryResult;
               // return Ok(ifexist);

                }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllProductionEntry action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_EntryDTO>)StatusCode(500, "Internal server error");
            }
        }





        [HttpGet("{labemp}")]///{compound}")]
        public async Task<IEnumerable<BNB_LogStatusDTO>> GetUpdatedProdEntryForLabEmp( string labemp)//, string compound)
        {

            try
            {
                var ifexist =await  _repository.BNBLogStatus.GetUpdatedProdEntryForLabEmp(labemp);//, compound);
                _logger.LogInfo($"Returned all ProductionEntry Updated by Lab from database.");
                var BNB_Production_EntryResult = _mapper.Map<IEnumerable<BNB_LogStatusDTO>>(ifexist);
                return BNB_Production_EntryResult;
                // return Ok(ifexist);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllProductionEntry action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_LogStatusDTO>)StatusCode(500, "Internal server error");
            }
        }

    }

 }
