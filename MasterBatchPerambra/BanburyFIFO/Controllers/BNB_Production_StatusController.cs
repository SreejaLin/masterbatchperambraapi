﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace ProdStatusFIFO.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class BNB_Production_StatusController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public BNB_Production_StatusController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {

            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]

        public async Task<IEnumerable<BNB_Production_StatusDTO>> GetAllProdStatuss()
        {
            try
            {
                var BNB_ProductionStatus = await _repository.BNBProductionStatus.GetAllproduction_Statuss();
                _logger.LogInfo($"Returned all ProdStatus from database.");
                var BNB_ProductionStatusResult = _mapper.Map<IEnumerable<BNB_Production_StatusDTO>>(BNB_ProductionStatus);
                return BNB_ProductionStatusResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllProdStatuss action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_Production_StatusDTO>)StatusCode(500, "Internal server error");
            }

        }


        [HttpPost]
        public IActionResult CreateProdStatus([FromBody] BNB_Production_StatusDTO ProdStatus)
        {
            try
            {
                if (ProdStatus == null)
                {
                    _logger.LogError("ProdStatus object sent from client is null.");
                    return BadRequest("ProdStatus object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid ProdStatus object sent from client.");
                    return BadRequest("Invalid model object");
                }


                // ProdStatus.ProdStatus_Id = new Guid();
                var ProdStatusEntity = _mapper.Map<BNBProductionStatus>(ProdStatus);




                _repository.BNBProductionStatus.Createproduction_Status(ProdStatusEntity);
                _repository.Save();

                var createdProdStatus = _mapper.Map<BNB_Production_StatusDTO>(ProdStatusEntity);
                return CreatedAtAction("GetAllproduction_Statuss", new { id = createdProdStatus.Production_Status_Id }, createdProdStatus);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateProductionStatus action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteProdStatus(int id)
        {
            try
            {
                var ProdStatus = _repository.BNBProductionStatus.Getproduction_StatusById(id);
                if (ProdStatus == null)
                {
                    _logger.LogError($"ProductionStatus with Code: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.BNBProductionStatus.Delete(ProdStatus);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public IActionResult UpdateProdStatus(int id, [FromBody] BNB_Production_StatusDTO ProdStatus)
        {
            try
            {
                if (ProdStatus == null)
                {
                    _logger.LogError("ProductionStatus object sent from client is null.");
                    return BadRequest("ProductionStatus object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid ProductionStatus object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var ProdStatusEntity = _repository.BNBProductionStatus.Getproduction_StatusById(id);
                if (ProdStatusEntity == null)
                {
                    _logger.LogError($"ProductionStatus with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(ProdStatus, ProdStatusEntity);
                _repository.BNBProductionStatus.Updateproduction_Status(ProdStatusEntity);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateProductionStatus action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
