﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace materialmappingFIFO.Controllers
{

        [ApiController]
        [Route("[controller]")]
        public class BNB_MaterialMappingController : ControllerBase
        {
            private ILoggerManager _logger;
            private IRepositoryWrapper _repository;
            private IMapper _mapper;


            public BNB_MaterialMappingController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
            {

                _logger = logger;
                _repository = repository;
                _mapper = mapper;
                _repository = repository;
            }
        [HttpGet]

        public async Task<IEnumerable<BNB_MaterialMappingDTO>> GetAllmaterialmappings()
        {
            try
            {
                var BNBMaterialMapping = await _repository.BNBMaterialMapping.GetAllMaterialMapping();
                _logger.LogInfo($"Returned all materialmapping from database.");
                var BNBMaterialMappingResult = _mapper.Map<IEnumerable<BNB_MaterialMappingDTO>>(BNBMaterialMapping);
                return BNBMaterialMappingResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllmaterialmappings action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_MaterialMappingDTO>)StatusCode(500, "Internal server error");
            }

        }
        [HttpGet("{CompoundCode}")]
        public Task<IEnumerable<BNB_MaterialMappingDTO>> GetMaterialMapping(string CompoundCode)
        {
            try
            {
                var BNBMaterialMapping = _repository.BNBMaterialMapping.GetMaterialMapping(CompoundCode);
                _logger.LogInfo($"Returned all ProductionEntryPending for approval from database.");
                var BNBMaterialMappingResult = _mapper.Map<IEnumerable<BNB_MaterialMappingDTO>>(BNBMaterialMapping);
                return Task.FromResult(BNBMaterialMappingResult);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetFiveLocationforcompounds action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return Task.FromResult((IEnumerable<BNB_MaterialMappingDTO>)StatusCode(500, "Internal server error"));
            }

        }


        [HttpPost]
        public IActionResult Creatematerialmapping([FromBody] BNB_MaterialMappingDTO materialmapping)
        {
            try
            {
                if (materialmapping == null)
                {
                    _logger.LogError("materialmapping object sent from client is null.");
                    return BadRequest("materialmapping object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid materialmapping object sent from client.");
                    return BadRequest("Invalid model object");
                }


                // materialmapping.materialmapping_Id = new Guid();
                var materialmappingEntity = _mapper.Map<BNBMaterialMapping>(materialmapping);




                _repository.BNBMaterialMapping.CreateMaterialMapping(materialmappingEntity);
                _repository.Save();

                var createdmaterialmapping = _mapper.Map<BNB_MaterialMappingDTO>(materialmappingEntity);
                return CreatedAtAction("GetAllmaterialmappings", new { id = createdmaterialmapping.Mapping_ID }, createdmaterialmapping);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Creatematerialmapping action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult Deletematerialmapping(int id)
        {
            try
            {
                var materialmapping = _repository.BNBMaterialMapping.GetMaterialMappingById(id);
                if (materialmapping == null)
                {
                    _logger.LogError($"materialmapping with Code: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.BNBMaterialMapping.Delete(materialmapping);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public IActionResult Updatematerialmapping(int id, [FromBody] BNB_MaterialMappingDTO materialmapping)
        {
            try
            {
                if (materialmapping == null)
                {
                    _logger.LogError("materialmapping object sent from client is null.");
                    return BadRequest("materialmapping object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid materialmapping object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var materialmappingEntity = _repository.BNBMaterialMapping.GetMaterialMappingById(id);
                if (materialmappingEntity == null)
                {
                    _logger.LogError($"materialmapping with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(materialmapping, materialmappingEntity);
                _repository.BNBMaterialMapping.UpdateMaterialMapping(materialmappingEntity);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }





    }

}
