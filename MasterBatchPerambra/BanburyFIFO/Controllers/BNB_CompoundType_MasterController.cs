﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using LoggerService;
using Entities;
using Entities.DataTransferObjects;
using AutoMapper;
using Entities.Models;

namespace BanburyFIFO.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BNB_CompoundType_MasterController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public BNB_CompoundType_MasterController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {

            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]

        public async Task<IEnumerable<BNB_CompoundType_MasterDTO>> GetAllCompoundType()
        {
            try
            {
                var BNB_CompoundType_Master = await _repository.BNBCompoundTypeMaster.GetAllCompoundType();
                _logger.LogInfo($"Returned all CompoundType from database.");
                var BNB_CompoundType_MasterResult = _mapper.Map<IEnumerable<BNB_CompoundType_MasterDTO>>(BNB_CompoundType_Master);
              
                return BNB_CompoundType_MasterResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllCompoundType action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<BNB_CompoundType_MasterDTO>)StatusCode(500, "Internal server error");
            }

        }


        [HttpPost]
        public IActionResult CreateCompoundType([FromBody] BNB_CompoundType_MasterDTO CompoundType)
        {
            try
            {
                if (CompoundType == null)
                {
                    _logger.LogError("CompoundType object sent from client is null.");
                    return BadRequest("CompoundType object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid CompoundType object sent from client.");
                    return BadRequest("Invalid model object");
                }
                //CompoundType.BNB_ShiftDate = DateTime.Today;
                //CompoundType.BNB_ShiftTime = DateTime.Now;

                // CompoundType.CompoundType_Id = new Guid();
                var CompoundTypeEntity = _mapper.Map<BNBCompoundTypeMaster>(CompoundType);

                _repository.BNBCompoundTypeMaster.CreateCompoundType(CompoundTypeEntity);
                _repository.Save();

                var createdCompoundType = _mapper.Map<BNB_CompoundType_MasterDTO>(CompoundTypeEntity);
                return CreatedAtAction("GetAllCompoundType", new { id = createdCompoundType.Compound_TypeId }, createdCompoundType);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateCompoundType action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteCompoundType(int id)
        {
            try
            {
                var CompoundType = _repository.BNBCompoundTypeMaster.GetCompoundTypeById(id);
                if (CompoundType == null)
                {
                    _logger.LogError($"CompoundType with Code: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.BNBCompoundTypeMaster.DeleteCompoundType(CompoundType);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public IActionResult UpdateCompoundType(int id, [FromBody] BNB_CompoundType_MasterDTO CompoundType)
        {
            try
            {
                if (CompoundType == null)
                {
                    _logger.LogError("CompoundType object sent from client is null.");
                    return BadRequest("CompoundType object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid CompoundType object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var CompoundTypeEntity = _repository.BNBCompoundTypeMaster.GetCompoundTypeById(id);
                if (CompoundTypeEntity == null)
                {
                    _logger.LogError($"CompoundType with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                //CompoundType.BNB_ShiftDate = DateTime.Today;
                //CompoundType.Modified_By = "Admin";
                _mapper.Map(CompoundType, CompoundTypeEntity);
                _repository.BNBCompoundTypeMaster.UpdateCompoundType(CompoundTypeEntity);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

       

    }
}
