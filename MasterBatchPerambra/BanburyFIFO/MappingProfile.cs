﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Models;
using Entities.DataTransferObjects;
using Entities;

namespace BanburyFIFO
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<BNBCompoundAgingRelaxation, BNB_Compound_Aging_RelaxationDTO>();
            CreateMap<BNB_Compound_Aging_RelaxationDTO, BNBCompoundAgingRelaxation>();

            CreateMap<BNBCompoundMaster, BNB_Compound_MasterDTO>()
                //.ForMember(destination => destination.Compound_typeName,
               //opts => opts.MapFrom(source => source.BNBCompoundTypeMaster.Compound_TypeName)).ForMember(destination => destination.Plant_Name,
             //  opts => opts.MapFrom(source => source.PlantMaster.Plant_Name))
               ;
            CreateMap<BNB_Compound_MasterDTO, BNBCompoundMaster>();

            CreateMap<BNBCompoundTypeMaster, BNB_CompoundType_MasterDTO>();
            CreateMap<BNB_CompoundType_MasterDTO, BNBCompoundTypeMaster>();

            CreateMap<BNBHoldReasons, BNB_Hold_ReasonsDTO>();
            CreateMap<BNB_Hold_ReasonsDTO, BNBHoldReasons>();

            CreateMap<BNBHold, BNB_HoldDTO>();
            CreateMap<BNB_HoldDTO, BNBHold>();

            CreateMap<BNBLogStatus, BNB_LogStatusDTO>()
                 .ForMember(destination => destination.Shift_Date, opts => opts.MapFrom(source => source.BNBProductionEntry.Created_Date.ToString("dd-MM-yyyy HH:mm:ss")))
                 .ForMember(destination => destination.Shift, opts => opts.MapFrom(source => source.BNBProductionEntry.Shift))
                 .ForMember(destination => destination.Compound_Code, opts => opts.MapFrom(source => source.BNBProductionEntry.Compound_Code))
                 .ForMember(destination => destination.Skid_NO, opts => opts.MapFrom(source => source.BNBProductionEntry.Skid_NO))
                 .ForMember(destination => destination.Batch_No, opts => opts.MapFrom(source => source.BNBProductionEntry.Batch_No))
                 .ForMember(destination => destination.Location, opts => opts.MapFrom(source => source.BNBProductionEntry.Location))
                 .ForMember(destination => destination.Prod_Status, opts => opts.MapFrom(source => source.BNBProductionEntry.Prod_Status))
                 .ForMember(destination => destination.Prod_Remarks, opts => opts.MapFrom(source => source.BNBProductionEntry.Prod_Remarks))
                 .ForMember(destination => destination.Lab_Status, opts => opts.MapFrom(source => source.BNBProductionEntry.Lab_Status))
                 .ForMember(destination => destination.Lab_Remarks, opts => opts.MapFrom(source => source.BNBProductionEntry.Lab_Remarks))
                 .ForMember(destination => destination.Banbury, opts => opts.MapFrom(source => source.BNBProductionEntry.Banbury))
                 ;
           CreateMap<BNB_LogStatusDTO, BNBLogStatus>();

            CreateMap<BNBLocationMaster, BNB_Location_MasterDTO>();
               //.ForMember(destination => destination.Plant_Name, opts => opts.MapFrom(source => source.PlantMaster.Plant_Name))
            CreateMap<BNB_Location_MasterDTO, BNBLocationMaster>();


            CreateMap<BNBMaster, BNB_MasterDTO>();
              // .ForMember(destination => destination.Plant_Name, opts => opts.MapFrom(source => source.PlantMaster.Plant_Name))
            CreateMap<BNB_MasterDTO, BNBMaster>();

            CreateMap<BNBProductionStatus, BNB_Production_StatusDTO>();
            CreateMap<BNB_Production_StatusDTO, BNBProductionStatus>();


            CreateMap<BNBUsers, BNB_UsersDTO>();
            CreateMap<BNB_UsersDTO, BNBUsers>();


            CreateMap<PlantMaster, PlantMasterDTO>();
            CreateMap<PlantMasterDTO, PlantMaster>();


            CreateMap<BNBProductionEntry, BNB_Production_EntryDTO>()
                .ForMember(destination => destination.bNBShiftDate,opts => opts.MapFrom(source => source.Created_Date.ToString("dd-MM-yyyy HH:mm:ss")))
                .ForMember(destination => destination.Prod_Remarks, opt => opt.NullSubstitute(""))
                .ForMember(destination => destination.Lab_Status, opt => opt.NullSubstitute(""))
                .ForMember(destination => destination.Lab_Remarks, opt => opt.NullSubstitute(""));
            CreateMap<BNB_Production_EntryDTO, BNBProductionEntry>();



            CreateMap<BNBProductionEntry, BNB_RmsDTO>().ForMember(destination => destination.bNBShiftDate, opts => opts.MapFrom(source => source.Created_Date.ToString("dd-MM-yyyy HH:mm:ss")));
            CreateMap<BNB_RmsDTO, BNBProductionEntry>();

            CreateMap<BNBTagGeneration, BNB_Tag_GenerationDTO>();
            CreateMap<BNB_Tag_GenerationDTO, BNBTagGeneration>();

            CreateMap<BNBMaterialMapping, BNB_MaterialMappingDTO>();
            CreateMap<BNB_MaterialMappingDTO, BNBMaterialMapping>();

            CreateMap<BNBSupplier, BNB_SupplierDTO>();
            CreateMap<BNB_SupplierDTO, BNBSupplier>();

        }



    }
}
//public UserProfile()

//{

//    CreateMap<User, UserViewModel>()

//    .ForMember(dest =>

//    dest.FName,

//    opt => opt.MapFrom(src => src.FirstName))

//    .ForMember(dest =>

//    dest.LName,

//    opt => opt.MapFrom(src => src.LastName))

//    .ReverseMap();

//}